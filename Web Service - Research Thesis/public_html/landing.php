<?php 
    include('config.php');
    
    session_start();
    if(!isset($_SESSION["username_web"]) && !isset($_SESSION["id_web"])) {
    header("Location: index.php");
    } else {
 	$start=0;
        $limit=12;
        if(isset($_GET['page']))
        {
            $page=$_GET['page'];
            $start=($page-1)*$limit;
        }
        else{
            $page=1;
        }

        $username_web = $_SESSION["username_web"];
        $id_web = $_SESSION["id_web"];
        $sql = "SELECT * FROM image INNER JOIN bible ON image.id = bible.id_image WHERE id_user = '$id_web' ORDER BY image.id DESC LIMIT $start, $limit";
        $result = mysqli_query($con,$sql);
    }

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Home</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/heroic-features.css" rel="stylesheet">

     <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="?page=1">Web Gambar Ayat Alkitab</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $username_web ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
<!--                         <li class="divider"></li> -->
                        <li>
                            <a href="logout_web.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">
        <!-- Title -->
        <div class="row">
            <div class="col-lg-12">
                <h3>Koleksi Gambar Ayat Alkitab</h3>
            </div>
        </div>
        <!-- /.row -->

        <!-- Page Features -->
        <div class="row text-center">

            <?php
            while ($check = mysqli_fetch_assoc($result)) {
            ?>

            <div class="col-md-3 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <a href="view_image.php?id_image=<?php echo $check['id_image'] ?>"> <img src="data:image/jpeg;base64, <?php echo $check['image_file'] ?>" alt="Gambar Ayat Alkitab"> </a>
                </div>
            </div>
            
            <?php    
             } 
            ?>

	    <?php
            $sql = "SELECT * FROM image INNER JOIN bible ON image.id = bible.id_image WHERE id_user = '$id_web' ORDER BY image.id DESC";
            $result = mysqli_query($con,$sql);

            $rows=mysqli_num_rows($result);
            $total=ceil($rows/$limit);

            echo "<div class='col-sm-12'>";
            echo "<ul class='pagination'>";
            for($i=1;$i<=$total;$i++)
            {
            if($i==$page) {echo "<li class='active'><a href='#'>".$i."</li>";}
            else {echo "<li><a href='?page=".$i."'>".$i."</a></li>";}
            }
            echo "</ul>";
            echo "<div>";

            ?>

            <!-- <div class="col-md-3 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <img src="http://placehold.it/800x500" alt="">
                    <div class="caption">
                        <h3>Feature Label</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <p>
                            <a href="#" class="btn btn-primary">Buy Now!</a> <a href="#" class="btn btn-default">More Info</a>
                        </p>
                    </div>
                </div>
            </div> -->
        </div>
        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Web Gambar Ayat Alkitab 2016 UKDW</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
