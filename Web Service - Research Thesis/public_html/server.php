<?php
	require 'function.php';
	require 'lib/nusoap.php';
	$server=new nusoap_server();
	$server->configureWSDL("xmlSignature","urn:demo");

	// $server->wsdl->addComplexType('hasil','complexType','struct','all','',
 //    array(  'hasilVerify'  => array('name' => 'hasilVerify','type' => 'xsd:string'),
 //            'token'      => array('name' => 'token','type' => 'xsd:string')
 //    ) );


	// $server->wsdl->addComplexType('hasil','complexType','array','sequence','',
 //  		array('hasilVerify' => array('type' => 'xsd:string', 'minOccurs' => '0', 'maxOccurs' => 'unbounded'),
 //  		'token' => array('type' => 'xsd:string', 'minOccurs' => '0', 'maxOccurs' => 'unbounded'))
	// );


	$server->register("validateXMLSignature", //name of function
				array("XMLSignature"=>'xsd:string', "username"=>'xsd:string', "token"=>'xsd:string'), // input
				array("return"=>'xsd:string') //output
				);

	$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
	$server->service($HTTP_RAW_POST_DATA);
?>