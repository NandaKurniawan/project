<?php 
    include('config.php');
    
    session_start();
    if(!isset($_SESSION["username_web"]) && !isset($_SESSION["id_web"])) {
    header("Location: index.php");
    } else {
        $username_web = $_SESSION["username_web"];
        $id_web = $_SESSION["id_web"];

        if (!isset($_GET['id_image'])) {
            header("Location: landing.php");
        } else {
            $id_image = $_GET['id_image'];
        }
        
        $sql = "SELECT * FROM image INNER JOIN bible ON image.id = bible.id_image WHERE image.id = '$id_image' ";
        $result = mysqli_query($con,$sql);
        $check = mysqli_fetch_assoc($result);
    }

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>View Image</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/heroic-features.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/portfolio-item.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Web Gambar Ayat Alkitab</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $username_web ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
<!--                         <li class="divider"></li> -->
                        <li>
                            <a href="logout_web.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <!-- Portfolio Item Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Gambar Nomor <?php echo $id_image ?> </h1>
            </div>
        </div>
        <!-- /.row -->

        <!-- Portfolio Item Row -->
        <div class="row">

            <div class="col-md-8">
                <img class="img-responsive" src="data:image/jpeg;base64, <?php echo $check['image_file'] ?>" alt="Gambar Ayat Alkitab">
            </div>

            <div class="col-md-4">
                <h3>Detail Ayat Alkitab :</h3>
                <ul>
                    <li>Kitab</li>
                        <ul><?php echo $check['kitab'] ?></ul>
                    <li>Pasal</li>
                        <ul><?php echo $check['pasal'] ?></ul>
                    <li>Ayat</li>
                        <ul><?php echo $check['ayat'] ?></ul>
                    <li>Isi</li>
                        <ul><?php echo $check['teks'] ?></ul>
		    <li>Link Bibles</li>
                        <ul><a href="<?php echo "http://bibles.org/eng-GNTD/".$check['book']."/".$check['pasal'] ?>" target='_blank'><?php echo $check['kitab'].' '.$check['pasal'].' : '.$check['ayat'] ?></a></ul>
                </ul>
                <h3>Detail Gambar :</h3>
                <ul>
                    <li>Make</li>
                        <ul><?php echo $check['make'] ?></ul>
                    <li>Model</li>
                        <ul><?php echo $check['model'] ?></ul>
                    <li>Exposure Time</li>
                        <ul><?php echo $check['exposure_time'] ?></ul>
                    <li>F_Number</li>
                        <ul><?php echo $check['f_number'] ?></ul>
                    <li>ISO</li>
                        <ul><?php echo $check['iso'] ?></ul>
                    <li>Date Taken</li>
                        <ul><?php echo $check['date_time_original'] ?></ul>
                    <li>Focal Length</li>
                        <ul><?php echo $check['focal_length'] ?></ul>
                    <li>Image Width</li>
                        <ul><?php echo $check['image_width'] ?></ul>
                    <li>Image Height</li>
                        <ul><?php echo $check['image_height'] ?></ul>    
                </ul>            
	    </div>
        </div>
        <!-- /.row -->

        <!-- Related Projects Row -->
        <!-- <div class="row">

            <div class="col-lg-12">
                <h3 class="page-header">Related Projects</h3>
            </div>

            <div class="col-sm-3 col-xs-6">
                <a href="#">
                    <img class="img-responsive portfolio-item" src="http://placehold.it/500x300" alt="">
                </a>
            </div>

            <div class="col-sm-3 col-xs-6">
                <a href="#">
                    <img class="img-responsive portfolio-item" src="http://placehold.it/500x300" alt="">
                </a>
            </div>

            <div class="col-sm-3 col-xs-6">
                <a href="#">
                    <img class="img-responsive portfolio-item" src="http://placehold.it/500x300" alt="">
                </a>
            </div>

            <div class="col-sm-3 col-xs-6">
                <a href="#">
                    <img class="img-responsive portfolio-item" src="http://placehold.it/500x300" alt="">
                </a>
            </div>

        </div> -->
        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Web Gambar Ayat Alkitab 2016 UKDW</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
