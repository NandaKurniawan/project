<?php

	function validateXMLSignature ($XMLSignature,$username,$token) {
		$bool = file_exists('logs/'.$username.'.txt');
		$fp = fopen('logs/'.$username.'.txt', 'a+');
		date_default_timezone_set("Asia/Jakarta");
		if ($bool) {
			fwrite($fp, "\n".'waktu akses web service '.date('Y-m-d H:i:s')."\n");
		} else {
			fwrite($fp, 'waktu akses web service '.date('Y-m-d H:i:s')."\n");
		}

		fwrite($fp, 'Data-data yang berhasil di terima yaitu Data XML : XML, Username : '.$username.' dan Token : '.$token."\n" );

		$server = "localhost";
		$username_server = "nanda";
		$pass = "nandaukdw2016";
		$db = "nanda";

		$con = mysqli_connect($server,$username_server,$pass,$db);

 		$pubkey = null;
 		$sql = "SELECT publicKey FROM user WHERE username = '$username' AND token = '$token' ";
 		$result = mysqli_query($con,$sql);
 		$check = mysqli_fetch_assoc($result);
		$pubkey = $check['publicKey'];
		//mysqli_close($con);

 		if (is_null($pubkey)) {
 			fwrite($fp, '1. Public Key tidak ditemukan berdasarkan username & token'."\n");
 			$token_baru = md5(uniqid(mt_rand(),true));
			$sql = "UPDATE user SET token = '$token_baru' WHERE username = '$username' AND token = '$token' ";
			$result = mysqli_query($con,$sql);
 			$arrayUser=array('hasil' => "Public key tidak ada", 'token' => $token_baru);
 			fwrite($fp, 'Sistem mengembalikan json yang berisi '.json_encode($arrayUser)."\n");
 			fclose($fp);
 			return json_encode($arrayUser);
 			//return "Public key tidak ada"; 
 		} else {
 			fwrite($fp, '1. Public Key ditemukan berdasarkan username & token'."\n");
 			$xml = simplexml_load_string($XMLSignature);
	 		list($hasilSHA1, $asliSHA1) = hitungUlangSHA1 ($xml);

			if($hasilSHA1==$asliSHA1){
				fwrite($fp, '2. Hash asli dan hash dari imageFile sama'."\n");	
				$pubkey = trim($pubkey);
				$key =  "-----BEGIN PUBLIC KEY-----\n".
		        chunk_split($pubkey, 64,"\n").
		       '-----END PUBLIC KEY-----';
		        $key = openssl_get_publickey($key);
				//$key = getPublicKey($pubKey); 

		        $signature = base64_decode(getSignatureValue($xml));
		        $SHA1 = base64_decode($asliSHA1);

				$hasilVerifyDigitalSignature = openssl_verify($SHA1, $signature, $key);
				if($hasilVerifyDigitalSignature){
					fwrite($fp, '3. Verifikasi Digital Signature berhasil (valid)'."\n");
					//-----------------------input ke database--------------------------------
					list($imageFile, $make, $model, $ex_time, $f_number, $iso, $date_time_ori, $f_length, $img_width, $img_height) = getDataImageXML($xml);
					list($book, $kitab, $pasal, $ayat, $teks, $f_horizontal, $f_vertical, $f_type, $f_color) = getDataBibleXML($xml);
					$sql = "SELECT id FROM user WHERE username = '$username' AND token = '$token' ";
 					$result = mysqli_query($con,$sql);
 					$check = mysqli_fetch_assoc($result);
 					$id_user = $check['id'];
 					
 					if (is_null($id_user)) {
 						fwrite($fp, '4. Pencarian id user berdasarkan username dan tokan tidak ditemukan'."\n");
 						$token_baru = md5(uniqid(mt_rand(),true));
						$sql = "UPDATE user SET token = '$token_baru' WHERE username = '$username' AND token = '$token' ";
						$result = mysqli_query($con,$sql);
			 			mysqli_close($con);
			 			$arrayUser=array('hasil' => "Signature valid namun id user tidak ada", 'token' => $token_baru);
			 			fwrite($fp, 'Sistem mengembalikan json yang berisi '.json_encode($arrayUser)."\n");
 						fclose($fp);
			 			return json_encode($arrayUser);
 						//return "Signature valid namun id user tidak ada";	
 					} else {
 						fwrite($fp, '4. Pencarian id user berdasarkan username dan tokan ditemukan'."\n");
 						//convert bas64 ke blob di ambil dari http://stackoverflow.com/questions/14044192/convert-base64-string-to-image-which-sent-from-android-using-json-and-save-as
 						$image_blob = base64_decode($imageFile);
 						//convert bas64 ke blob di ambil dari http://stackoverflow.com/questions/14044192/convert-base64-string-to-image-which-sent-from-android-using-json-and-save-as

 						//--------------------------insert ke image-------------------
 						// $sql = "INSERT INTO image (id_user, image_file, make, model, exposure_time, f_number, iso, date_time_original, focal_length, image_width, image_height) VALUES ('$id_user', '$image_blob','$make', '$model', '$ex_time', '$f_number', $iso, $date_time_ori, $f_length, $img_width, $img_height)";
 						$sql = "INSERT INTO image (id_user, image_file, make, model, exposure_time, f_number, iso, date_time_original, focal_length, image_width, image_height) VALUES ('$id_user', '$imageFile', '$make', '$model', '$ex_time', '$f_number', '$iso', '$date_time_ori', '$f_length', '$img_width', '$img_height')";
 						//masalah di image membuat tidak bisa masuk ke DB!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
						$result = mysqli_query($con,$sql);
						// return "'$id_user', '$make', '$model', '$ex_time', '$f_number', $iso, '$date_time_ori', '$f_length', '$img_width', '$img_height'";
 						
						//--------------------------insert ke image-------------------
						if ($result) {
							fwrite($fp, '5. Input data-data gambar berdasarkan id user ke database tabel image sukses'."\n");
							//--------------------------cari id gambar-------------------
							// $sql = "SELECT id FROM image WHERE id_user='id_user' AND make='$make' AND model='$model' AND exposure_time='$ex_time' AND f_number='$f_number'AND iso='$iso' AND date_time_original='$date_time_ori' AND focal_length='$f_length' AND image_width='$img_width' AND image_height='$img_height' ";
							$sql = "SELECT id FROM image ORDER BY id  DESC LIMIT 1";
							$result = mysqli_query($con,$sql);
							$check = mysqli_fetch_assoc($result);
							$id_image = $check['id'];
							//--------------------------cari id gambar-------------------
							if (is_null($id_image)) {
								fwrite($fp, '6. Pencarian id image berdasarkan id terakhir id image tidak ditemukan'."\n");
								$token_baru = md5(uniqid(mt_rand(),true));
								$sql = "UPDATE user SET token = '$token_baru' WHERE username = '$username' AND token = '$token' ";
								$result = mysqli_query($con,$sql);
					 			mysqli_close($con);
					 			$arrayUser=array('hasil' => "Id image tidak dapat dicari kembali", 'token' => $token_baru);
					 			fwrite($fp, 'Sistem mengembalikan json yang berisi '.json_encode($arrayUser)."\n");
 								fclose($fp);
					 			return json_encode($arrayUser);
								//return "Id image tidak dapat dicari kembali";
							} else{
								fwrite($fp, '6. Pencarian id image berdasarkan id terakhir id image ditemukan'."\n");
								// $sql = "INSERT INTO bible (id_image, kitab, pasal, ayat, teks, book, font_horizontal, font_vertical, font_type, font_color) VALUES ('$id_image', '$kitab', '$pasal', '$ayat', '$teks', '$book', '$f_horizontal', '$f_vertical', '$f_type', '$f_color')";
								$sql = "INSERT INTO bible (id_image, kitab, pasal, ayat, teks, book, font_horizontal, font_vertical, font_type, font_color) VALUES ('$id_image', '$kitab', '$pasal', '$ayat', '$teks', '$book', '$f_horizontal', '$f_vertical', '$f_type', '$f_color')";
								$result = mysqli_query($con,$sql);
								if ($result) {
									fwrite($fp, '7. Input data-data ayat alkitab berdasarkan id image ke database tabel image sukses'."\n");
									$token_baru = md5(uniqid(mt_rand(),true));
									$sql = "UPDATE user SET token = '$token_baru' WHERE username = '$username' AND token = '$token' ";
									$result = mysqli_query($con,$sql);
						 			mysqli_close($con);
									date_default_timezone_set("Asia/Jakarta");
						 			$arrayUser=array('hasil' => "Signature berhasil di verifikasi (valid) dan data berhasil ditulis di database", 'token' => $token_baru, 'time' => date('Y-m-d H:i:s'));
						 			fwrite($fp, 'Sistem mengembalikan json yang berisi '.json_encode($arrayUser)."\n");
 									fclose($fp);
						 			return json_encode($arrayUser);
									//return "Signature berhasil di verifikasi (valid) dan data berhasil ditulis di database";
								} else {
									fwrite($fp, '7. Input data-data ayat alkitab berdasarkan id ke database tabel image gagal'."\n");
									$token_baru = md5(uniqid(mt_rand(),true));
									$sql = "UPDATE user SET token = '$token_baru' WHERE username = '$username' AND token = '$token' ";
									$result = mysqli_query($con,$sql);
						 			mysqli_close($con);
						 			$arrayUser=array('hasil' => "Signature berhasil di verifikasi (valid) tetapi data tidah berhasil ditulis didatabase", 'token' => $token_baru);
						 			fwrite($fp, 'Sistem mengembalikan json yang berisi '.json_encode($arrayUser)."\n");
 									fclose($fp);
						 			return json_encode($arrayUser);
									//return "Signature berhasil di verifikasi (valid) tetapi data tidah berhasil ditulis didatabase";
								}								
							}
						} else {
							fwrite($fp, '5. Input data-data gambar berdasarkan id user ke database tabel image gagal'."\n");
							$token_baru = md5(uniqid(mt_rand(),true));
							$sql = "UPDATE user SET token = '$token_baru' WHERE username = '$username' AND token = '$token' ";
							$result = mysqli_query($con,$sql);
						 	mysqli_close($con);
						 	$arrayUser=array('hasil' => "Insert ke tabel image gagal", 'token' => $token_baru);
						 	fwrite($fp, 'Sistem mengembalikan json yang berisi '.json_encode($arrayUser)."\n");
 							fclose($fp);
						 	return json_encode($arrayUser);
							//return "Insert ke tabel image gagal";
						}
 					}
					// //-----------------------input ke database--------------------------------
					// $updateToken = updateToken($username, $token);
					//return "Signature berhasil di verifikasi (valid).......";
				} else {
					fwrite($fp, '3. Verifikasi Digital Signature tidak berhasil (tidak valid)'."\n");
					$token_baru = md5(uniqid(mt_rand(),true));
					$sql = "UPDATE user SET token = '$token_baru' WHERE username = '$username' AND token = '$token' ";
					$result = mysqli_query($con,$sql);
				 	mysqli_close($con);
				 	$arrayUser=array('hasil' => "Signature gagal di verifikasi", 'token' => $token_baru);
				 	fwrite($fp, 'Sistem mengembalikan json yang berisi '.json_encode($arrayUser)."\n");
 					fclose($fp);
				 	return json_encode($arrayUser);
					//return "Signature gagal di verifikasi";
				}
			} else {
				fwrite($fp, '2. Hash asli dan hash dari imageFile tidak sama'."\n");
				$token_baru = md5(uniqid(mt_rand(),true));
				$sql = "UPDATE user SET token = '$token_baru' WHERE username = '$username' AND token = '$token' ";
				$result = mysqli_query($con,$sql);
			 	mysqli_close($con);
			 	$arrayUser=array('hasil' => "Hasil SHA-1 tidak sama", 'token' => $token_baru);
			 	fwrite($fp, 'Sistem mengembalikan json yang berisi '.json_encode($arrayUser)."\n");
 				fclose($fp);
			 	return json_encode($arrayUser);
				//return "Hasil SHA-1 tidak sama";
			}
		}
	}

	//diambil dari http://php.net/manual/en/function.fwrite.php
	// function tulisFile ($XML) {
	// 	$myfile = fopen("coba.xml", "w");
	// 	fwrite($myfile, $XML);
	// 	unset($myFile);
	// }

	//---------------------------------------------------------	

	//diambil dari http://stackoverflow.com/questions/20146977/how-to-convert-public-key-generation-from-java-to-php
	// function getPublicKey($pubkey){
	// 	$pubkey = trim($pubkey);
	// 	$key =  "-----BEGIN PUBLIC KEY-----\n".
 //        chunk_split($pubkey, 64,"\n").
 //       '-----END PUBLIC KEY-----';
 //        $key = openssl_get_publickey($key);
 //        return $key;
	// }
	//---------------------------------------------------------	

	function hitungUlangSHA1 ($xml) {
		$valueDigest = getDigestValue($xml);
		$valueImage = getImageValue($xml);

		//fungsi trim dimbil dari http://php.net/manual/en/function.trim.php
		//fungsi hash diambil dari http://php.net/manual/en/function.hash.php
		$trimText = trim($valueImage);
		$hasilHash = hash('sha1',$trimText);

		$trimText2 = trim($valueDigest);
		
		return array($hasilHash, $trimText2);
	}

	//diambil dari http://php.net/manual/en/simplexmlelement.xpath.php & http://www.w3schools.com/xsl/xpath_syntax.asp & http://www.hackingwithphp.com/12/3/3/searching-and-filtering-with-xpath
	function getDigestValue ($xml) {
		$digest = $xml->xpath('//DigestValue');
		foreach ($digest as $digestt) {
			$valueDigest = $digestt;
		}
		return $valueDigest;
	}

	function getSignatureValue ($xml) {
		$signature = $xml->xpath('//SignatureValue');
		foreach ($signature as $signaturee) {
			$valueSignature = $signaturee;
		}
		return $valueSignature;
	}

	function getImageValue ($xml) {
		$image = $xml->xpath('//imageFile');
		foreach ($image as $imageFile) {
			$valueImage = $imageFile;
		}
		return $valueImage;
	}
	//---------------------------------------------------------	

	//---------------------------------------------------------
	function getDataImageXML($xml){
		$exif = $xml->xpath('//exif');
		foreach ($exif as $result) {
			$Make = $result->Make;
			$Model = $result->Model;
			$ExposureTime = $result->ExposureTime;
			$FNumber = $result->FNumber;
			$ISO = $result->ISO;
			$DateTimeOriginal = $result->DateTimeOriginal;
			$FocalLength = $result->FocalLength;
			$ImageWidht = $result->ImageWidht;
			$ImageHeight = $result->ImageHeight;
		}

		$valueImage = getImageValue($xml);
		$trimText = trim($valueImage);

		return array($trimText, $Make, $Model, $ExposureTime, $FNumber, $ISO, $DateTimeOriginal, $FocalLength, $ImageWidht, $ImageHeight);
	}

	function getDataBibleXML($xml){
		$item = $xml->xpath('//item');
		foreach ($item as $result) {
			$book = $result->book;
			$bookname = $result->bookname;
			$chapter = $result->chapter;
			$verse = $result->verse;
			$text = $result->text;	
		}

		$font = $xml->xpath('//font');
		foreach ($font as $result) {
			$horizontal = $result->horizontal;
			$vertical = $result->vertical;
			$type = $result->type;
			$color = $result->color;
		}

		return array($book, $bookname, $chapter, $verse, $text, $horizontal, $vertical, $type, $color);
	}
	//---------------------------------------------------------

	//---------------------------------------------------------
	// function updateTokn ($username,$token,$con){
	// 	$newToken = md5(uniqid(mt_rand(),true));
	// 	$sql = "UPDATE user SET token = '$newToken' WHERE username = '$username' AND token = '$token' ";
	// 	$result = mysqli_query($con,$sql);

	// 	if ($result) {
	// 		return $newToken;
	// 	} else {
	// 		return "Update token gagal";
	// 	}
		
	// }
	//---------------------------------------------------------


?>