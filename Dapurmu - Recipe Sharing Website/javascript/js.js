$(document).ready(function() {
	//login
	$('a.login-window').click(function() {
		
		// Getting the variable's value from a link 
		var loginBox = $(this).attr('href');

		//Fade in the Popup and add close button
		$(loginBox).fadeIn(300);
		
		//Set the center alignment padding + border
		var popMargTop = ($(loginBox).height() + 24) / 2; 
		var popMargLeft = ($(loginBox).width() + 24) / 2; 
		
		$(loginBox).css({ 
			'margin-top' : -popMargTop,
			'margin-left' : -popMargLeft
		});
		
		// Add the mask to body
		$('body').append('<div id="mask"></div>');
		$('#mask').fadeIn(300);
		
		return false;
	});


	//register
	$('a.register-window').click(function() {
		
		// Getting the variable's value from a link 
		var registerBox = $(this).attr('href');

		//Fade in the Popup and add close button
		$(registerBox).fadeIn(300);
		
		//Set the center alignment padding + border
		var popMargTop = ($(registerBox).height() + 24) / 2; 
		var popMargLeft = ($(registerBox).width() + 24) / 2; 
		
		$(registerBox).css({ 
			'margin-top' : -popMargTop,
			'margin-left' : -popMargLeft
		});
		
		// Add the mask to body
		$('body').append('<div id="mask"></div>');
		$('#mask').fadeIn(300);
		
		return false;
	});


	//forgot
	$('a.forgot-window').click(function() {
		
		// Getting the variable's value from a link 
		var forgotBox = $(this).attr('href');

		//Fade in the Popup and add close button
		$(forgotBox).fadeIn(300);
		
		//Set the center alignment padding + border
		var popMargTop = ($(forgotBox).height() + 24) / 2; 
		var popMargLeft = ($(forgotBox).width() + 24) / 2; 
		
		$(forgotBox).css({ 
			'margin-top' : -popMargTop,
			'margin-left' : -popMargLeft
		});
		
		// Add the mask to body
		$('body').append('<div id="mask"></div>');
		$('#mask').fadeIn(300);
		
		$('.login-popup').fadeOut(300); 

		return false;

	});


	
	// When clicking on the button close or the mask layer the popup closed
	$('a.close, #mask').live('click', function() { 
	  $('#mask , .login-popup, .register-popup, .forgot-popup').fadeOut(300 , function() {
		$('#mask').remove();  
	}); 
	return false;
	});

	
});