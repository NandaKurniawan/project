<?php
    session_start();
    if($_SESSION['email']== ""){
    header('Location: index.php');
    }
    include "koneksi.php";
	include "src/loged/header.php";
    include "src/loged/upside.php";
	include "src/loged/navbar-home.php";
?>
	<div id="layout">
   		<div id="content_post">

        <form method="post" enctype="multipart/form-data" class="posting-now" action="posting.php">
        <table id="posting-receipt">
            <tr>
                <td><h2>What's your recipe today?</h2></td>
            </tr>
            <tr>
                <td>
                	Add Photo: <input class="button-at-home" type ="file" name="food_pict" id="recipe-picture"><div class="batasan">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspMax. 2MB (jpg,png)</div>
                    <input class="food-name" type="text" name="food_name" placeholder="Food name">
                    <select name="category">
                        <option value="sayuran">Sayuran</option>
                        <option value="seafood">Ikan dan Seafood</option>
                        <option value="daging">Daging</option>
                        <option value="kedelai">Olahan Kedelai</option>
                        <option value="umbi">Umbi-umbian</option>
                        <option value="kuah">Makanan berkuah</option> 
                        <option value="pendamping">Makanan pendamping</option>
                        <option value="mie">Mie dan Pasta</option>
                        <option value="kue">Kue</option>
                        <option value="roti">Roti dan Pastri</option>
                        <option value="minuman">Minuman</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <input class="description-food" type="text" name="description" placeholder="Description">
                </td>
            </tr>
            <tr>
                <td>Ingredients:</td>
            </tr>
            <tr>
                <td id="td_ingredients">
                    <div id="ingredients0">
                        <input class="pcs-class" type="text" name="ingredient[0][pcs]" placeholder="Pcs">
                        <input class="ingredients-class" type="text" name="ingredient[0][name]" placeholder="Type ingredient">
                        <input id="add-ingredients0" class="add-posting-button" type="button" value="+" >
                    </div>					
                </td>
            </tr>
            <tr>
                <td>How to cook this:</td>
            </tr>
            <tr>
                <td id="td_howto">
                    <div id="howto0">
                        <input class="howto-class" type="text" name="howto[0][desc]" placeholder="Step 1">
                        <input id="add-howto0" class="add-posting-button" type="button" value="+" >
                        <span>Attach Image: </span><input class="button-at-home" type ="file" name="howto[0][pict]"><div class="batasan">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspMax. 2MB (jpg,png)</div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <button class="posting-button" type="submit" id="post-recipe-button">Post</button>
                </td>
            </tr>
        </table>
        </form>

        <!--tampilan resep-->
        <div id="isi">
			<?php 
            $getid = "select id from user where email = '".$_SESSION['email']."'";
            $querygetid = mysql_query($getid,$con);
            $hasilgetid = mysql_fetch_assoc($querygetid);
            $id = $hasilgetid["id"];
    
            if (isset($_GET["category"])){ 
                    $category = $_GET["category"];
                    if (isset($_GET["page"])){
                            $page=$_GET["page"];
                        }
                    else{
                        $page=1;
                    }   


                    $limit = 9;
                    $start = $limit*($page-1);
            
                    $sqldb = "select * from resep where kategori = '".$category."' ORDER BY id DESC limit $start,$limit";
                    $tot = mysql_query("select * from resep where kategori = '".$category."'");
                    $total = mysql_num_rows($tot);
                    $num_page = ceil($total/$limit);
            
                    $getresep = mysql_query($sqldb,$con);?>

                    <div id="bungkusresep">
                    <?php while ($resep = mysql_fetch_array($getresep)) { ?>
                            
                                <div class="resep">
                                    <div class="gambarresep"><a href="showreseps.php?idresep=<?php echo $resep['id'] ?>"><?php echo '<img class="gambar" src="data:image/jpeg;base64,'.base64_encode($resep["picture"]).'" onerror="this.src='.$defaultresep.'" alt="Recipe Photo" >' ?> </a> </div>
                                    <div class="judulresep"><a href="showreseps.php?idresep=<?php echo $resep['id'] ?>" class="judulresepp" ><?php echo $resep["judul_resep"] ?></a></div>
                                </div>
                            
                    <?php } ?>
                    </div>

                    <div id="page">
                    <?php
                    if($num_page>=1){
                        for ($i=1; $i <= $num_page ; $i++) { 
                            if ($i==$page) {
                                echo '<a href="home.php?category='.$category.'&page='.$i.'" class="pagee"">'.$i.'</a>';
                            }
                            else{
                                echo '<a href="home.php?category='.$category.'&page='.$i.'" class="pageee"">'.$i.'</a>';
                            }
            
                        }
                        echo"</div>";
                    }
                    ?>  
                    </div>

           <?php }

            else {
                    if (isset($_GET["page"])){
                            $page=$_GET["page"];
                        }
                    else{
                        $page=1;
                    }
                     
                    $limit = 9;
                    $start = $limit*($page-1);
            
                    $sqldb = "select * from resep ORDER BY id DESC limit $start,$limit";
                    $tot = mysql_query("select * from resep");
                    $total = mysql_num_rows($tot);
                    $num_page = ceil($total/$limit);
            
                    $getresep = mysql_query($sqldb,$con);?>

                    <div id="bungkusresep">
                    <?php while ($resep = mysql_fetch_array($getresep)) { ?>
                            
                                <div class="resep">
                                    <div class="gambarresep"><a href="showreseps.php?idresep=<?php echo $resep['id'] ?>"><?php echo '<img class="gambar" src="data:image/jpeg;base64,'.base64_encode($resep["picture"]).'" onerror="this.src='.$defaultresep.'"  alt="Recipe Photo" >' ?> </a> </div>
                                    <div class="judulresep"><a href="showreseps.php?idresep=<?php echo $resep['id'] ?>" class="judulresepp" ><?php echo $resep["judul_resep"] ?></a></div>
                                </div>
                            
                    <?php }  ?>
                    </div>

                    <div id="page">
                    <?php
                    if($num_page>=1){
                        for ($i=1; $i <= $num_page ; $i++) { 
                            if ($i==$page) {
                                echo '<a href="home.php?page='.$i.'" class="pagee"">'.$i.'</a>';
                            }
                            else{
                                echo '<a href="home.php?page='.$i.'" class="pageee"">'.$i.'</a>';
                            }
            
                        }
                        echo"</div>";
                    }
                    ?>
                    </div>
            <?php } ?>


            </div>
        </div>
    </div>
        <!--pagination-->



   		<div id="rightbar">
     		<div id="tasteoftheweek">
     			<h2 class="title">Taste of the Week</h2>
     			<?php
                $sqldb = "select * from resep ORDER BY like_count DESC limit 0,4";
                $getresep = mysql_query($sqldb,$con)
                ?>

                <div id="bungkustasteoftheweek">
                    <?php while ($resep = mysql_fetch_array($getresep)) { ?>
                                <div class="reseptasteoftheweek">
                                    <div class="judulreseptasteoftheweek"><a href="showreseps.php?idresep=<?php echo $resep['id'] ?>" class="judulresepptasteoftheweek" ><?php echo $resep["judul_resep"] ?></a></div>
                                </div>
                    <?php } ?>
                </div>
     		</div>
     		<div id="category">
     			<h2 class="title">Category</h2>
     			<ul>
     				<li><a href="home.php?category=sayuran">Sayuran</a></li>
     				<li><a href="home.php?category=seafood">Ikan dan Seafood</a></li>
     				<li><a href="home.php?category=daging">Daging</a></li>
     				<li><a href="home.php?category=kedelai">Olahan Kedelai</a></li>
     				<li><a href="home.php?category=umbi">Umbi-umbian</a></li>
					<li><a href="home.php?category=kuah">Makanan berkuah</a></li> 
                    <li><a href="home.php?category=pendamping">Makanan pendamping</a></li>
                    <li><a href="home.php?category=mie">Mie dan Pasta</a></li>
                    <li><a href="home.php?category=kue">Kue</a></li>
                    <li><a href="home.php?category=roti">Roti dan Pastri</a></li>
                    <li><a href="home.php?category=minuman">Minuman</a></li>

     			</ul>
     		</div>
   		</div>
	</div>



<?php	
	include "src/footer.php";
?>