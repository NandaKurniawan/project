<!--NAVBAR-->
<?php
	include "koneksi.php";
?>

<div id="all-navbar">
	<div id="list-navbar">
		<ul class="navbar">
			<li class="active"><a href="index.php">Home</a></li>
			<li class="trik"><a href="trik.php">TIPS & TRICK</a></li>
			<li class="about"><a href="about.php">ABOUT</a></li>
		</ul>
	</div>
	<div id="search-navbar">
		<form>
			<div class="search-field">
				<input type="text" onclick="this.value=&#39;&#39;;" onfocus="this.select()" onblur="this.value=!this.value?&#39;Search Resep&#39;:this.value;" value="Search Resep" id="keyword">
				<input class="search-button" type="image" src="img/search-icon-1.png" alt="search">
			</div>
		</form>
	</div>
</div>

<div id="slide-search-result">
    	
</div>

<script>
	$(document).ready(function() {
		var resultBox = document.getElementById("slide-search-result");
		
		$("#slide-search-result").bind('mouseleave', function(){$(resultBox).fadeOut(300);});
		
		$("#keyword").keyup(function(){
			var keyword = document.getElementById("keyword").value;
			if(keyword != ""){
				$.ajax({
					url: 'ajaxsearch.php?keyword=' + keyword + '&type=resep',
					type: 'GET',
					success: function(data) {
						var srchResult = document.getElementById("slide-search-result");
						while (srchResult.firstChild) {
							srchResult.removeChild(srchResult.firstChild);
						}
						$("#slide-search-result").append(data);
					}
				});
				
				//Fade in the Popup and add close button
				$(resultBox).css({
					'margin-top' : -18
				});
				$(resultBox).fadeIn(300);
			}	
			return false;
		});
	});
</script>