<body>
<div id="container">
<header>
    <a id="webname" href="index.php"><img src="img/logo.png" alt="dapurmu.com"></a>
	   <a href="catalog.php?onmonth=1" class="catalogue"><img src="img/katalog.png"  alt="catalogue"></a>
    <a href="#register-box" class="register-window"><img src="img/reg.png"  alt="register_now"></a>  
    <a href="#login-box" class="login-window"><img src="img/log.png" alt="login"></a>


	


	<div id="login-box" class="login-popup">
        <a href="#" class="close"><img src="img/close_pop.png" class="btn_close" title="Close Window" alt="Close" /></a>
          <form method="post" id="login" class="signin" action="proses-login.php">
                <fieldset class="textbox">
            	
                <label class="email">
                <span>Email</span>
                <input id="email-login" name="email" value="" type="text" autocomplete="on" placeholder="Email">
                </label>
                
                <label class="password">
                <span>Password</span>
                <input id="password-login" name="password" value="" type="password" placeholder="Password">
                </label>
                
                 <?php if (isset($_GET["err"])) {
					 		if ( $_GET["err"] == 1 ) {
				 				echo '<div class="err1">*Email or Password incorrect</div>';
							}
				 }?>
                     
                
                <button class="login-submit button" type="submit">Sign in</button>
                
                <p>
                <a class="forgot-window" href="#forgot-box">Forgot your password?</a>
                </p>
                
                </fieldset>
          </form>

        <script type="text/javascript">
           var frmvalidatorLogin  = new Validator("login");

           frmvalidatorLogin.addValidation("email","req","Please enter your Email");
           frmvalidatorLogin.addValidation("email","email","Please enter a valid Email");
           frmvalidatorLogin.addValidation("password","req","Please enter your Password");

        </script>

		</div>




    <div id="register-box" class="register-popup">
        <a href="#" class="close"><img src="img/close_pop.png" class="btn_close" title="Close Window" alt="Close" /></a>
          <form method="post" id="register" class="signin" action="proses-register.php">
                <fieldset class="textbox">
                
                <label class="name">
                <span>Name</span>
                <input id="name-register" name="name" value="" type="text" autocomplete="on" placeholder="Name">
                </label>

                <label class="email-register">
                <span>Email</span>
                <input id="email" name="email" value="" type="text" autocomplete="on" placeholder="Email">
                </label>
                
                <label class="password">
                <span>Password</span>
                <input id="password-register" name="password" value="" type="password" placeholder="Password">
                </label>

                <label class="retype-password">
                <span>Retype Password</span>
                <input id="retype-password-register" name="retype-password" value="" type="password" placeholder="Retype-Password">
                </label>

                <label class="secret-question">
                <span>Secret Question</span>
                <select id="secret-question-login" name="secret-question">
                    <option value="0" selected="selected">Choose One</option>
                    <option value="food">What your favourite food?</option>
                    <option value="hobby">What is your hobby?</option>
                    <option value="colour">What is your favourite colour?</option>
                    <option value="pet">What is name of your pet?</option>
                    <option value="girlfriend">What is your girlfriend's name?</option>
                    <option value="boyfriend">What is your boyfriend's name?</option>
                    <option value="mother">What is your mother's name?</option>
                </select>

                </label>

                <label class="answer">
                <span>Answer</span>
                <input id="answer-register" name="answer" value="" type="text" placeholder="answer">
                </label>
                
                <button class="register-submit button" type="submit">Sign Up</button>
                <!--
                <p>
                <a class="forgot" href="#">Forgot your password?</a>
                </p>
                -->
                </fieldset>
          </form>

        <script type="text/javascript">
           var frmvalidatorRegister  = new Validator("register");

           frmvalidatorRegister.addValidation("name","req","Please enter your Name");
           frmvalidatorRegister.addValidation("email","req","Please enter your Email");
           frmvalidatorRegister.addValidation("email","email","Please enter a valid Email");
           frmvalidatorRegister.addValidation("password","req","Please enter your Password");
           frmvalidatorRegister.addValidation("retype-password","req","Please enter your Verified Password");
           frmvalidatorRegister.addValidation("password","eqelmnt=retype-password","The confirmed password is not same as password");
           frmvalidatorRegister.addValidation("secret-question","dontselect=0");
           
        </script>

        </div>


    <div id="forgot-box" class="forgot-popup">
        <a href="#" class="close"><img src="img/close_pop.png" class="btn_close" title="Close Window" alt="Close" /></a>
          <form method="post" id="recovery" class="signin" action="proses-recovery.php">
                <fieldset class="textbox">

                <label class="email">
                <span>Email</span>
                <input id="email-forgot" name="email" value="" type="text" autocomplete="on" placeholder="Email">
                </label>              

                <label class="secret-question">
                <span>Secret Question</span>
                <select id="secret-question" name="secret-question">
                    <option value="0" selected="selected">Choose One</option>
                    <option value="food">What your favourite food?</option>
                    <option value="hobby">What is your hobby?</option>
                    <option value="colour">What is your favourite colour?</option>
                    <option value="pet">What is name of your pet?</option>
                    <option value="girlfriend">What is your girlfriend's name?</option>
                    <option value="boyfriend">What is your boyfriend's name?</option>
                    <option value="mother">What is your mother's name?</option>
                </select>
                </label>

                <label class="answer">
                <span>Answer</span>
                <input id="answer-forgot" name="answer" value="" type="text" placeholder="answer">
                </label>

                <label class="password">
                <span>New Password</span>
                <input id="password-forgot" name="password" value="" type="password" placeholder="Password">
                </label>

                <label class="retype-password">
                <span>Retype New Password</span>
                <input id="retype-password-forgot" name="retype-password" value="" type="password" placeholder="Retype-Password">
                </label>
                
                <button class="forgot-submit button" type="submit">Recover</button>
                <!--
                <p>
                <a class="forgot" href="#">Forgot your password?</a>
                </p>
                -->
                </fieldset>
          </form>

         <script type="text/javascript">
           var frmvalidatorRecovery  = new Validator("recovery");

           frmvalidatorRecovery.addValidation("email","req","Please enter your Email");
           frmvalidatorRecovery.addValidation("email","email","Please enter a valid Email");
           frmvalidatorRecovery.addValidation("secret-question","dontselect=0","Please select the Question");
           frmvalidatorRecovery.addValidation("answer","req","Please enter your Answer");
           frmvalidatorRecovery.addValidation("password","req","Please enter your Password");
           frmvalidatorRecovery.addValidation("retype-password","req","Please enter your Verified Password");
           frmvalidatorRecovery.addValidation("password","eqelmnt=retype-password","The confirmed password is not same as password");
           
           
            </script>

        </div>

</header>