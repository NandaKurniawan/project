<?php
	session_start();
	
	include "src/header-about.php";
	include "src/upside.php";
	include "src/navbar-about.php";
?>
	<div id="layout">
   		<div id="content_about">
     		<p id="aboutus">ABOUT US</p>

     		<div id="isiabout">
     			<div id="break"></div>
     			

     			<div id="content-isiabout">
     				Apa itu dapurmu?</br></br>
     				Dapurmu adalah tempat membuat, menyimpan dan sharing resep masakanmu. Kami menyediakan antar muka yang nyaman saat menulis resep sehingga tampilan resepnya akan menjadi rapi. Kami juga menyediakan tempat untuk berhubungan antar koki di website ini melalui fitur pertemanan. Melalui fitur pertemanan ini pengguna bisa saling berkirim pesan dan melihat resep dari koki lain secara ekslusif. Fitur lain yang ada di website kami adalah rank. Rank ini didasarkan pada jumlah like (yummy) pada setiap resep. Resep dengan jumlah like terbanyak akan tampil di "Taste of The Week" dan "Catalogue" setiap bulannya.
     				</br><br>

					Dapatkan pengalaman berharga melalui website kami. Happy cooking :)
     				
     			</div>


     		</div>

   		</div>
	</div>

<?php	
	include "src/footer.php";
?>