<?php
    session_start();
    if($_SESSION['email']== ""){
    header("location: index.php");
    }
    include "koneksi.php";
    include "src/loged/header.php";
    include "src/loged/upside.php";
    include "src/loged/navbar-home.php";

    if (isset($_GET["idresep"])){
    	$idresep=$_GET["idresep"];
    }

    $sqldb = "select * from resep where id = ".$idresep."";
    $getresep = mysql_query($sqldb,$con);
    while ($resep = mysql_fetch_array($getresep)) {
    	$namaresep = $resep['judul_resep'];
    	$deskripsiresep = $resep['deskripsi'];
    	$gambarresep = $resep['picture'];
    	$tanggalpost = $resep['tgl_post'];
    	$iduser = $resep['id_user'];
    }

    $sqldb1 = "select * from user where id = $iduser";
    $getnama = mysql_query($sqldb1,$con);
    while ($name = mysql_fetch_array($getnama)){
    	$namauser = $name['nama'];
    	$gambaruser = $name['picture'];
    }

?>
	
	<div id="layout">
   		<div id="content_showresep">
			
            <div id="up-side">
            	<div id="nama-resep">
					<?php echo $namaresep; ?>
                </div>

                <div id="user">
                    <div id="foto-user">
                        <?php echo '<img class="foto" src="data:image/jpeg;base64,'.base64_encode($gambaruser).'" onerror="this.src='.$defaultorang.'" alt="Photo Profile">' ?>
                    </div>
                    <div id="username">
                        by Chef: </br>
                        <?php echo $namauser ?> </br>
                        <?php echo $tanggalpost ?>
                    </div>
                </div>

          <?php
          $query = "select * from user where email = '".$_SESSION["email"]."'";
          $hasilquery = mysql_query($query,$con);
          $hasil = mysql_fetch_assoc($hasilquery);
          $id = $hasil['id'];

          if($id!=$iduser) { ?> 
              <?php

              $query = "select * from friend where id_user = $id and id = $iduser";
              $hasilquery = mysql_query($query,$con);
              $hasilquery = mysql_fetch_assoc($hasilquery);
              if ($hasilquery == true){ ?>
                  <div id="add">
                        <button id="add-friend"><a href="proses-friend.php?id_user=<?php echo $id ?>&id=<?php echo $iduser ?>&resep=<?php echo $idresep ?>">Unfriend</a></button>
                  </div>
              <?php }
              else{ ?>
                  <div id="add">
                        <button id="add-friend"><a href="proses-friend.php?id_user=<?php echo $id ?>&id=<?php echo $iduser ?>&resep=<?php echo $idresep ?>">Add Friend</a></button>
                  </div>
              <?php } ?>
          <?php } else{?> 
               <div id="add">
                        <button id="add-friend"><a href="proses-delete.php?id_resep=<?php echo $idresep ?>">Delete</a></button>  
          <?php } ?>  
                
          
            </div>

   			<div id="clear"></div>
   			
   			<div id="left-side">
   				<div id="foto">
   					<?php echo '<img class="ppfood" src="data:image/jpeg;base64,'.base64_encode($gambarresep).'" onerror="this.src='.$defaultresep.'" alt="Photo Resep">' ?>
   				</div>
   				<div id="ingre">
                	<br>
   					<span class=section-head>Ingredients:</span>

            <div id="ingres">
                    <?php
						$query = "select nama_bahan, jumlah from bahan where id_resep = '$idresep'";
    					$getdata = mysql_query($query,$con);
						$ingre = 1;
   						while ($data = mysql_fetch_array($getdata)){
      						$desc = $data['nama_bahan'];
     						$jml = $data['jumlah'];
							echo '<div class=ingres>
										<div><div class="ingre-head">Ingredient '.$ingre.'</div> <div class="ingre-desc">'.$jml.'  '.$desc.'</div></div>
								  </div>';
							$ingre += 1;
   					 	}
					?>
                    </div>
   				</div>

   			</div>
   			<div id="right-side">
   				<div id="desc">
                	<br>
   					<span class=section-head>Description:</span>
                    
            <!--LIKE-->
            <?php 
            $query2 = "select * from `like` where id_user = $id and id_resep = $idresep";
            $hasilquery1 = mysql_query($query2,$con);
            $hasilquery2 = mysql_num_rows($hasilquery1);

              if ($hasilquery2==1){ ?>
                  <div class="like">
                    <img src="img/unlike.png" id="imglike">
                  </div>
              <?php }
              else{ ?>
                  <div class="like">
                    <img src="img/like.png" id="imglike">
                  </div>
              <?php } ?>
             
            <script>
				$('.like').click(function(){
					$.ajax({
						url: 'proses-like.php?iduser=' + <?php echo $id ?> + '&idresep=' + <?php echo $idresep ?>,
						type: 'GET',
						success: function(data) {
							if(document.getElementById("imglike").getAttribute("src") == "img/like.png"){
								document.getElementById("imglike").setAttribute("src", "img/unlike.png");
							} else {
								document.getElementById("imglike").setAttribute("src", "img/like.png");
							}
						}
					});
				});	
            </script>
            <!--LIKE-->

		            <?php echo '<br><div id=recipe-desc>'.$deskripsiresep.'</div>'?>
   				</div>
   				<div id="howto">
                	<br>
   					<span class=section-head>How to cook this:</span>
                    <div id="steps">
                    <?php
						$query = "select isi, picture from langkah where id_resep = '$idresep'";
    					$getdata = mysql_query($query,$con);
						$step = 1;
   						while ($data = mysql_fetch_array($getdata)){
      						$desc = $data['isi'];
     						$pict = $data['picture'];
							echo '<div class=steps>
										<img class="step-picts" src="data:image/jpeg;base64,'.base64_encode($pict).'" onerror="this.src='.$defaultstep.'" alt=" Foto Step">
										<div><div class="step-head">Step '.$step.'</div> <br><br> <div class="step-desc">'.$desc.'</div></div>
								  </div>';
							$step += 1;
   					 	}
					?>
                    </div>
   				</div>
   			</div>

   		
   			<div id="clear"></div>
        
       		<div id="comment-section">
   				<br>
   				<span class=section-head>Comments</span>
                <div id="comments">
                    <?php
						$query = "select * from comment where id_resep = '$idresep'";
    					$getdata = mysql_query($query,$con);
   						while ($data = mysql_fetch_array($getdata)){
      						$desc = $data['isi'];
							$tgl = $data['tgl_comment'];
							$id = $data['id_user'];
							$query = "select nama, picture from user where id = '$id'";
    						$result = mysql_query($query,$con);
  							$result = mysql_fetch_array($result);
							echo '<div class=comments>
										<img class="cmt-userpicts" src="data:image/jpeg;base64,'.base64_encode($result['picture']).'" onerror="this.src='.$defaultorang.'" alt="Photo Profile">
										<div><div class="cmt-username">'.$result['nama'].'</div> <div class="cmt-date">'.$tgl.'</div><div class="cmt-desc">'.$desc.'</div></div>
								  </div>';
   					 	}
					?>
                </div>
   				<input class="input-comment" type=text id="comment-text" placeholder="Type comment here..." align="top"></input>
   				<input class="post-button" type="button" value="Post"/>
   			</div>
            
            <script>
            	$('.post-button').click(function(){
					var resep = document.getElementById("nama-resep").textContent;
					var comment = document.getElementById("comment-text").value;
					$.ajax({
						url: 'ajaxresep.php?comment=' + comment + '&resep=' + resep,
						type: 'GET',
						success: function(data) {
							$("#comments").append(data);
							$("#comment-text").value = "";
							$("#comment-text").text = "";
							$("#comment-text").textContent = "";
						}
					});
				});
            </script>

   		</div>
	</div>

<?php	
	include "src/footer.php";
?>