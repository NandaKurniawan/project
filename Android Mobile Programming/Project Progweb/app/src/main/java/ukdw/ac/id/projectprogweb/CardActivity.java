package ukdw.ac.id.projectprogweb;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;


public class CardActivity extends RecyclerView.Adapter<CardActivity.PostsViewHolder>{
    List<Posts> postses;
    Bitmap gambarGondes;
    //Class<HomeActivity> homeActivityClass;
    Context context;
    //static String id;
    static SharedPreferences sharedPreferences;

    CardActivity(List<Posts> postses, Context context, SharedPreferences sharedpreferences){
        this.postses= postses;
        //this.homeActivityClass = homeActivityClass;
        this.context = context;
        this.sharedPreferences = sharedpreferences;
    }

    @Override
    public PostsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_post, viewGroup, false);
        PostsViewHolder pvh = new PostsViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PostsViewHolder personViewHolder, final int i) {

        gambarGondes = decodeBase64(postses.get(i).getImage_user());

        personViewHolder.nama.setText(postses.get(i).getId_user());
        personViewHolder.judul.setText(postses.get(i).getJudul());
        personViewHolder.deskripsi.setText(postses.get(i).getDeskripsi());
        personViewHolder.personPhoto.setImageBitmap(gambarGondes);
        final String id = postses.get(i).getId_posting();

        personViewHolder.fl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                attPost.setIdPost(id);
//                Toast.makeText(context, attPost.getIdPost(), Toast.LENGTH_LONG).show();

                Intent intent = new Intent(context, ViewPostActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return postses.size();
    }

    public class PostsViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView nama, judul, deskripsi;
        FrameLayout fl;

        ImageView personPhoto;

        PostsViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);
            nama = (TextView)itemView.findViewById(R.id.nameUser);
            judul = (TextView)itemView.findViewById(R.id.judulPost);
            deskripsi = (TextView)itemView.findViewById(R.id.deskripsiPost);
            personPhoto = (ImageView)itemView.findViewById(R.id.imgUser);
            fl = (FrameLayout)itemView.findViewById(R.id.fl);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static Bitmap decodeBase64(String input)
    {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }
}


