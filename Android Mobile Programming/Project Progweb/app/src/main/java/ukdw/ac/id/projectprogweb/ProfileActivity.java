package ukdw.ac.id.projectprogweb;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;


public class ProfileActivity extends ActionBarActivity {

    //private SharedPreferences sharedpreferences;"
    public static final String MyPREFERENCES = "MyPrefs" ;
    SharedPreferences sharedPreferences;
//    SharedPreferences.Editor editor = sharedPreferences.edit();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        TextView name = (TextView)findViewById(R.id.txtNamaUser);


        sharedPreferences = getSharedPreferences(MyPREFERENCES, 0);
        name.setText(sharedPreferences.getString("name",null));

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void post(View view){
        Intent intent = new Intent(this,PostActivity.class);
        startActivity(intent);
    }
}
