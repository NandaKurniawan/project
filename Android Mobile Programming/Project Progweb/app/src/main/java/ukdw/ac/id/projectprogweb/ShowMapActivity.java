package ukdw.ac.id.projectprogweb;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


public class ShowMapActivity extends Activity implements LocationListener {

    Intent intent;

    private GoogleMap googleMap;
    LocationManager lm;
    Location location;
    Double latitudeC ;
    Double longitudeC ;
    MarkerOptions marker2;
    LatLng latlng;
    String namaTempat;
    String description;

    private static final long MIN_TIME = 400;
    private static final float MIN_DISTANCE = 1000;

    private SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_map);

        intent = getIntent();
        latitudeC = Double.valueOf(intent.getStringExtra("latitudeMap"));
        longitudeC = Double.valueOf(intent.getStringExtra("longitudeMap"));

        try {
            initializeMap();
        }
        catch (Exception e){
            e.printStackTrace();
        }

        location = new Location("");
        location.setLatitude(latitudeC);
        location.setLongitude(longitudeC);

        onLocationChanged(location);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_show_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLocationChanged(Location location) {
        latitudeC = location.getLatitude();
        longitudeC = location.getLongitude();
        latlng = new LatLng(latitudeC, longitudeC);

//            marker2 = new MarkerOptions().position(new LatLng(latitudeC, longitudeC)).title(""+"my location").snippet("posisi saya di lat : "+latitudeC+" long : "+longitudeC).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
//            googleMap.addMarker(marker2);

        Marker marker1 = googleMap.addMarker(new MarkerOptions().position(new LatLng(latitudeC, longitudeC)));

        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latlng, 15);
        googleMap.animateCamera(cameraUpdate);
//        marker1.hideInfoWindow();
        marker1.showInfoWindow();

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private void initializeMap() {
        if (googleMap == null){
            googleMap = ((MapFragment)getFragmentManager().findFragmentById(R.id.map)).getMap();
            if (googleMap == null){
                Toast.makeText(getApplicationContext(), "Sorry! Unable to create maps", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
