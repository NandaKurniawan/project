package ukdw.ac.id.projectprogweb;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;


public class ViewPostActivity extends ActionBarActivity {

    private SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;

//    Intent intent = getIntent();
//    Intent intent = new Intent(getApplicationContext(), ViewPostActivity.class);
//    String id = intent.getExtras().getString("id");
//    String id = intent.getStringExtra("id");

    String id;
    String judul_posting, diskripsi_posting, tempat, waktu, latitude, longitude, picture, username;

    ImageView imgEvent;
    TextView lblJudulEvent, txtPlace, txtTime, txtDescription;
    Button btnMap;

    boolean bPicture=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_post);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();

        imgEvent = (ImageView)findViewById(R.id.imgEvent);
        lblJudulEvent = (TextView)findViewById(R.id.lblJudulEvent);
        txtPlace = (TextView)findViewById(R.id.txtPlace);
        txtTime = (TextView)findViewById(R.id.txtTime);
        btnMap = (Button)findViewById(R.id.btnMaps);
        txtDescription = (TextView)findViewById(R.id.txtDescription);

        id=attPost.getIdPost();
//        Toast.makeText(getApplicationContext(), id, Toast.LENGTH_LONG).show();

        new showPost(id).execute();

        btnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ShowMapActivity.class);
                intent.putExtra("latitudeMap", latitude);
                intent.putExtra("longitudeMap", longitude);
                //Toast.makeText(getApplicationContext(), latitude+", "+longitude,Toast.LENGTH_LONG).show();
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_post, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static Bitmap decodeBase64(String input)
    {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }

    public class showPost extends AsyncTask<String, Void, JSONObject> {

        private JSONObject jsonObject;
        String idPost;

        public showPost(String id) {
            this.idPost = id;
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            DefaultHttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://192.168.137.1/progmob/index.php");

            try {

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("tag", "showPost"));
                nameValuePairs.add(new BasicNameValuePair("idPost", idPost));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                HttpResponse httpResponse = httpclient.execute(httppost);
                HttpEntity httpEntity = httpResponse.getEntity();
                String data = EntityUtils.toString(httpEntity);

                JSONObject jObj = new JSONObject(data);

                judul_posting = jObj.getString("judul_posting");
                diskripsi_posting = jObj.getString("diskripsi_posting");
                tempat = jObj.getString("tempat");
                waktu = jObj.getString("waktu");
                if (!(jObj.getString("latitude").equals("null"))&&!(jObj.getString("longitude").equals("null"))){
                    latitude = jObj.getString("latitude");
                    longitude = jObj.getString("longitude");
                }
                else{
                    latitude = null;
                    longitude = null;
                }
                bPicture = false;
                if (!jObj.getString("picture").equals("null")){
                    picture = jObj.getString("picture");
                    bPicture=true;
                }

                username = jObj.getString("username");

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject content){

//            Toast.makeText(getApplicationContext(), idPost,Toast.LENGTH_LONG).show();
//            Toast.makeText(getApplicationContext(), judul_posting+" "+tempat+" "+waktu+" "+diskripsi_posting,Toast.LENGTH_LONG).show();
            lblJudulEvent.setText(judul_posting);
            txtPlace.setText(tempat);
            txtTime.setText(waktu);
//            if (latitude.equals("null")&&longitude.equals("null")){
//                btnMap.setClickable(false);
//            }
//            else{
//                btnMap.setClickable(true);
//            }
            txtDescription.setText(diskripsi_posting);
            if (bPicture) {
                Bitmap bitmap = decodeBase64(picture);
                imgEvent.setImageBitmap(bitmap);
            }
            //Toast.makeText(getApplicationContext(), picture,Toast.LENGTH_LONG).show();
        }
    }



}
