package ukdw.ac.id.projectprogweb;

/**
 * Created by Adhya Krisananta on 15/05/15.
 */
public class Posts {
    private String id_posting;
    private String id_user;
    private String judul;
    private String deskripsi;
    private String image_user;
    private String username;
//    private String image_user;

    public String getImage_user() {
        return image_user;
    }

    public void setImage_user(String image_user) {
        this.image_user = image_user;
    }

    public Posts(String id_posting, String id_user, String judul, String deskripsi, String image_user){
        this.id_posting = id_posting;
        this.id_user = id_user;
        this.judul = judul;
        this.deskripsi = deskripsi;
        this.image_user = image_user;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public String getId_posting() {
        return id_posting;
    }

    public String getId_user() {
        return id_user;
    }

    public String getJudul() {
        return judul;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public void setId_posting(String id_posting) {
        this.id_posting = id_posting;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

}

