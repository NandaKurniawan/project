package ukdw.ac.id.projectprogweb;

import android.graphics.Bitmap;

/**
 * Created by Adhya Krisananta on 20/05/15.
 */
public class PostData {
    static String id_user, judul_posting, diskripsi_posting, tempat, waktu, picture;

    public PostData(String id, String s, String s1, String s2, String s3, String s4) {
        this.id_user = id;
        this.diskripsi_posting = s1;
        tempat = s2;
        waktu = s3;
        picture = s4;
    }

    //getter

    public static String getId_user() {
        return id_user;
    }

    public static String getJudul_posting() {
        return judul_posting;
    }

    public static String getDiskripsi_posting() {
        return diskripsi_posting;
    }

    public static String getTempat() {
        return tempat;
    }

    public static String getWaktu() {
        return waktu;
    }

    public static String getPicture() {
        return picture;
    }

    //setter
    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public void setJudul_posting(String judul_posting) {
        this.judul_posting = judul_posting;
    }

    public void setDiskripsi_posting(String diskripsi_posting) {
        this.diskripsi_posting = diskripsi_posting;
    }

    public void setTempat(String tempat) {
        this.tempat = tempat;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
