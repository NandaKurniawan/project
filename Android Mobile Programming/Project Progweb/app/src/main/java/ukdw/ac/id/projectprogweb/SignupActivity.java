
package ukdw.ac.id.projectprogweb;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SignupActivity extends ActionBarActivity {

    private EditText txtName, txtUsername, txtPassword, txtEmail, txtPassword2;
    private String nama, username, password, email, password2, response;
    private ProgressDialog pDialog;
    private SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        txtName=(EditText)findViewById(R.id.txtName);
        txtUsername=(EditText)findViewById(R.id.txtUsername);
        txtEmail=(EditText)findViewById(R.id.txtEmail);
        txtPassword=(EditText)findViewById(R.id.txtPassword);
        txtPassword2=(EditText)findViewById(R.id.txtRetryPassword);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_signup, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void signUp(View view) throws UnsupportedEncodingException {
        username=txtUsername.getText().toString();
        nama=txtName.getText().toString();
        email=txtEmail.getText().toString();
        password=txtPassword.getText().toString();
        password2=txtPassword2.getText().toString();
//        new TestPost().execute();

        if (nama.equals("")||username.equals("")||email.equals("")){
            Toast.makeText(this, "Please fill all the information", Toast.LENGTH_SHORT).show();
        }
        else if(!password.equals(password2)){
            Toast.makeText(this, "Password and Retry Password don't match", Toast.LENGTH_SHORT).show();
        }
        else {
            new TestPost(username, nama, email, password).execute();
            Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();
            Intent intent= new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        }
    }

    private class TestPost extends AsyncTask<Void, Void, Void >{

        public TestPost(String username, String nama, String email, String password) {

        }

        @Override
        protected Void doInBackground(Void... params) {
            registerUser(username, nama, email, password);
            return null;
        }
    }

//PILIHAN KE DUA -----------------------------------------------------------------------------------------------------------------------------------------------------------

    public void registerUser(String nama, String username, String email, String password) {
        try {
            //instantiates httpclient to make request
            DefaultHttpClient httpclient = new DefaultHttpClient();

            //url with the post data
            HttpPost httppost = new HttpPost("http://192.168.137.1/progmob/index.php");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(5);
            nameValuePairs.add(new BasicNameValuePair("tag", "register"));
            nameValuePairs.add(new BasicNameValuePair("username", username));
            nameValuePairs.add(new BasicNameValuePair("nama", nama));
            nameValuePairs.add(new BasicNameValuePair("email", email));
            nameValuePairs.add(new BasicNameValuePair("password", password));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            // Execute HTTP Post Request
            HttpResponse response = httpclient.execute(httppost);
            String content = EntityUtils.toString(response.getEntity());
            Log.d("TEST", content);


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }  catch (IOException e) {
            e.printStackTrace();
        }

    }


//PILIHAN PERTAMA ----------------------------------------------------------------------------------------------------------------------------------------------------------
//    public void registerUser (String nama, String username, String email, String password) throws UnsupportedEncodingException {
//
//        String data = URLEncoder.encode("tag", "UTF-8") + "=" + URLEncoder.encode("register","UTF-8");
//        data += URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(username,"UTF-8");
//        data += URLEncoder.encode("nama", "UTF-8") + "=" + URLEncoder.encode(nama,"UTF-8");
//        data += URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(email,"UTF-8");
//        data += URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(password,"UTF-8");
//
//        String text = "";
//        BufferedReader reader = null;
//
////        send data
//
//        try {
//            //url
//            URL url = new URL("http://192.168.137.1/progmob/index.php");
//
//            // Send POST data request
//
//            URLConnection conn = url.openConnection();
//            conn.setDoOutput(true);
//            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
//            wr.write( data );
//            wr.flush();
//
//            // Get the server response
//
//            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
//            StringBuilder sb = new StringBuilder();
//            String line = null;
//
//            // Read Server Response
//            while((line = reader.readLine()) != null)
//            {
//                // Append server response in string
//                sb.append(line + "\n");
//            }
//
//
//            text = sb.toString();
//        }
//        catch(Exception ex)
//        {
//
//        }
//        finally
//        {
//            try
//            {
//
//                reader.close();
//            }
//
//            catch(Exception ex) {}
//        }
//
//        Toast.makeText(this, data.toString(),Toast.LENGTH_LONG).show();
//    }

//PILIHAN KE TIGA --------------------------------------------------------------------------------------------------------------------------------

//    private void registerUser(final String name, final String email, final String password) {
//        // Tag used to cancel the request
//        String tag_string_req = "req_register";
//
//        pDialog.setMessage("Registering ...");
//        showDialog();
//
//        StringRequest strReq = new StringRequest(Request.Method.POST,AppConfig.URL_REGISTER, new Response.Listener<String>() {
//
//            @Override
//            public void onResponse(String response) {
//                Log.d(TAG, "Register Response: " + response.toString());
//                hideDialog();
//
//                try {
//                    JSONObject jObj = new JSONObject(response);
//                    boolean error = jObj.getBoolean("error");
//                    if (!error) {
//                        // User successfully stored in MySQL
//                        // Now store the user in sqlite
//                        String uid = jObj.getString("uid");
//
//                        JSONObject user = jObj.getJSONObject("user");
//                        String name = user.getString("name");
//                        String email = user.getString("email");
//                        String created_at = user
//                                .getString("created_at");
//
//                        // Inserting row in users table
//                        db.addUser(name, email, uid, created_at);
//
//                        // Launch login activity
//                        Intent intent = new Intent(
//                                RegisterActivity.this,
//                                LoginActivity.class);
//                        startActivity(intent);
//                        finish();
//                    } else {
//
//                        // Error occurred in registration. Get the error
//                        // message
//                        String errorMsg = jObj.getString("error_msg");
//                        Toast.makeText(getApplicationContext(),
//                                errorMsg, Toast.LENGTH_LONG).show();
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.e(TAG, "Registration Error: " + error.getMessage());
//                Toast.makeText(getApplicationContext(),
//                        error.getMessage(), Toast.LENGTH_LONG).show();
//                hideDialog();
//            }
//        }) {
//
//            @Override
//            protected Map<String, String> getParams() {
//                // Posting params to register url
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("tag", "register");
//                params.put("name", name);
//                params.put("email", email);
//                params.put("password", password);
//
//                return params;
//            }
//
//        };
//
//        // Adding request to request queue
//        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
//    }

}
