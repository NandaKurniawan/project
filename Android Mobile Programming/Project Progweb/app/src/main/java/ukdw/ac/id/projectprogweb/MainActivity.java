package ukdw.ac.id.projectprogweb;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class MainActivity extends ActionBarActivity {

    private String username, password, nama;
    private EditText txtUsername, txtPassword;
    private SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;

    private int id;
    private boolean error;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedpreferences=getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        Button btnLogin = (Button) findViewById(R.id.btnLogin);
        Button btnSignUp = (Button) findViewById(R.id.btnSignUp);
        txtUsername=(EditText)findViewById(R.id.txtUsername);
        txtPassword=(EditText)findViewById(R.id.txtPassword);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void signup(View view){
        Intent intent = new Intent(this, SignupActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        sharedpreferences=getSharedPreferences(MyPREFERENCES,
                Context.MODE_PRIVATE);
        if (sharedpreferences.contains("id"))
        {

                Intent i = new Intent(this, HomeActivity.class);
                startActivity(i);
                finish();

        }
        super.onResume();
    }

    public void login(View view){
        username=txtUsername.getText().toString();
        password=txtPassword.getText().toString();

        if (username.equals("")||password.equals("")){
            Toast.makeText(this, "Username or Password empty", Toast.LENGTH_SHORT).show();
        }
        else{
            new checkLogin(username, password).execute();
        }
    }

    private class checkLogin extends AsyncTask<String, String, JSONObject> {

        private String checkUsername, checkPassword;
        private JSONObject jsonObject;
        StringBuilder builder = new StringBuilder();

        public checkLogin(String username, String password) {
            checkUsername = username;
            checkPassword = password;
        }

        @Override
        protected JSONObject doInBackground(String... params) {

            DefaultHttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://192.168.137.1/progmob/index.php");
            //HttpPost httppost = new HttpPost("http://124.81.118.213:7777/~gs14c5/progmob/index.php");

            try {

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("tag", "login"));
                nameValuePairs.add(new BasicNameValuePair("username", checkUsername));
                nameValuePairs.add(new BasicNameValuePair("password", checkPassword));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));


                HttpResponse httpResponse = httpclient.execute(httppost);
                HttpEntity httpEntity = httpResponse.getEntity();
                String data = EntityUtils.toString(httpEntity);

                //data = getJ(httppost);

                JSONObject jObj = new JSONObject(data);

                error = jObj.getBoolean("error");
                if (!error){
                    id = jObj.getInt("id");
                    username = jObj.getString("username");
                    nama = jObj.getString("nama");
                }

//                HttpResponse response = httpclient.execute(httppost);
//                HttpEntity entity = response.getEntity();

//                JSONObject jsonObject = null;
//                StringBuilder builder = new StringBuilder();
//                try {
//                    HttpResponse response = httpclient.execute(httppost);
//                    StatusLine statusLine = response.getStatusLine();
//                    int statusCode = statusLine.getStatusCode();
//                    if (statusCode == 200)
//                    {
//                        HttpEntity entity = response.getEntity();
//                        InputStream content = entity.getContent();
//                        BufferedReader reader = new BufferedReader(new InputStreamReader(content));
//                        String line;
//                        while ((line = reader.readLine()) != null) {
//                            builder.append(line);
//                        }
//
//                        String a = EntityUtils.toString(entity);
//                        //jsonObject = new JSONObject(builder.toString());
//                        jsonObject = new JSONObject(a);
//
//                        error = jsonObject.getBoolean("error");
//                        //username = jsonObject.getString("id");
//                    }
//                } catch (ClientProtocolException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                 catch (JSONException e) {
//                    e.printStackTrace();
//                }

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }  catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jsonObject;

        }

        @Override
        protected void onPostExecute(JSONObject content){
            if (!error){
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putInt("id", id);
                editor.putString("username", username);
                editor.putString("nama", nama);
                editor.commit();

//                Toast.makeText(getApplicationContext(), "Loggin Success", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(intent);
                finish();
            }
            else{
                Toast.makeText(getApplicationContext(),"Username or Password Wrong!!!", Toast.LENGTH_LONG).show();
            }
        }
    }
}

// Sumber
// http://www.tutorialspoint.com/android/android_session_management.htm
// http://stackoverflow.com/questions/7479992/handling-a-menu-item-click-event-android

//      SharedPreferences.Editor editor = sharedpreferences.edit();
//      editor.putString(name,username);
//      editor.putString(pass,password);
//      editor.commit();
//
//      Intent intent = new Intent(this, HomeActivity.class);
//      startActivity(intent);
//      finish();
