package ukdw.ac.id.projectprogweb;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


public class ChoseMapActivity extends Activity implements LocationListener {

    //Google Map
    private GoogleMap googleMap;
    LocationManager lm;
    Location location;
    Double latitudeC =-7.8032504;
    Double longitudeC = 110.3748449;

    Double lathasil, longhasil;

    MarkerOptions marker2;
    Marker marker1;
    LatLng latlng;

    private static final long MIN_TIME = 400;
    private static final float MIN_DISTANCE = 1000;

    Button btnOKE,btnCancel;

    private SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chose_map);

        btnOKE=(Button)findViewById(R.id.btnOke);
        btnCancel=(Button)findViewById(R.id.btnCancel);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();

        try {
            //Loading map
            initializeMap();
        }
        catch (Exception e){
            e.printStackTrace();
        }

        location = new Location("");
        location.setLatitude(latitudeC);
        location.setLongitude(longitudeC);

        onLocationChanged(location);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chose_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void sendLocation(View view){
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("latitude", String.valueOf(lathasil));
        editor.putString("longitude", String.valueOf(longhasil));
        editor.commit();
        Toast.makeText(this, lathasil+" "+longhasil,Toast.LENGTH_LONG).show();

        Intent intent = new Intent (getApplicationContext(), PostActivity.class);
//        intent.putExtra("latitude",lathasil);
//        intent.putExtra("longitude",longhasil);
        startActivity(intent);
        finish();
    }

    public void cancelLocation(View view){
        Intent intent = new Intent (getApplicationContext(), PostActivity.class);
        startActivity(intent);
        finish();
    }



    @Override
    public void onLocationChanged(Location location) {
//        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
//        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng,10);
//        googleMap.animateCamera(cameraUpdate);
//        lm.removeUpdates(this);

        latitudeC = location.getLatitude();
        longitudeC = location.getLongitude();
        latlng = new LatLng(latitudeC, longitudeC);

        if (marker2 == null){
            marker1 =googleMap.addMarker(new MarkerOptions().position(new LatLng(latitudeC, longitudeC)).draggable(true));

            googleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener(){

                @Override
                public void onMarkerDragStart(Marker arg0) {

                }

                @Override
                public void onMarkerDrag(Marker arg0) {

                }

                @Override
                public void onMarkerDragEnd(Marker arg0) {
                    LatLng markerLocation = marker1.getPosition();
                    lathasil = markerLocation.latitude;
                    longhasil = markerLocation.longitude;
                    //Toast.makeText(ChoseMapActivity.this, lathasil + " " + longhasil, Toast.LENGTH_LONG).show();
                }
            });

//            googleMap.addMarker(marker2);

            //when the location changes, update the map by zooming to the location
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latlng, 10);
            googleMap.animateCamera(cameraUpdate);
        }
        else{
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latlng, 10);
            googleMap.animateCamera(cameraUpdate);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private void initializeMap() {
        if (googleMap == null){
            googleMap = ((MapFragment)getFragmentManager().findFragmentById(R.id.map)).getMap();
            if (googleMap == null){
                Toast.makeText(getApplicationContext(), "Sorry! Unable to create maps", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public boolean isLocation(){
        boolean enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return enabled;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode == 1){
            if (isLocation()==true){
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        }
    }
}
