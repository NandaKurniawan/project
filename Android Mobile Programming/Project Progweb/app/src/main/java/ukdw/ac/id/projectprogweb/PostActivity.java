package ukdw.ac.id.projectprogweb;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class PostActivity extends ActionBarActivity {

    EditText nameEvent, placeEvent, timeEvent, latitude, longitude, deskripsiEvent;
    ImageView imgEvent;
    Button maps, image, makeEvent;
    Uri selectedImage, fileUri;
    String linkImage="";
    String realPath, picture = "";
//    String latitude, longitude;
    int mode=0, id;

    public static final int media_foto = 1;
    private static final int foto_code = 100;
    private static final int media_code = 1000;

    private SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;

    boolean edit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();

        nameEvent = (EditText)findViewById(R.id.txtNameEvent);
        placeEvent = (EditText)findViewById(R.id.txtPlaceEvent);
        timeEvent = (EditText)findViewById(R.id.txtTimeEvent);
        latitude = (EditText)findViewById(R.id.txtLatitude);
        longitude = (EditText)findViewById(R.id.txtLongtitude);
        deskripsiEvent = (EditText)findViewById(R.id.txtDescriptionEvent);

        maps = (Button)findViewById(R.id.btnAddMapsEvent);
        image = (Button)findViewById(R.id.btnPhotosEvent);
        makeEvent = (Button)findViewById(R.id.btnSubmitEvent);

        imgEvent = (ImageView)findViewById(R.id.imageEvent);

        id = sharedpreferences.getInt("id", 0);

        makeEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(latitude.getText().toString().equals("")||longitude.getText().toString().equals("")){
                    latitude.setText("null");
                    longitude.setText("null");
                }
                preparingToUpload(v);

            }
        });

        maps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stringId = String.valueOf(id);
                new PostData(stringId, nameEvent.getText().toString(), deskripsiEvent.getText().toString(),
                        placeEvent.getText().toString(), timeEvent.getText().toString(), picture);
                Intent intent = new Intent(getApplicationContext(), ChoseMapActivity.class);
                startActivity(intent);
            }
        });

        if(sharedpreferences.contains("latitude")&&sharedpreferences.contains("longitude")){
            id = Integer.parseInt(PostData.getId_user());
            nameEvent.setText(PostData.getJudul_posting());
            deskripsiEvent.setText(PostData.getDiskripsi_posting());
            placeEvent.setText(PostData.getTempat());
            timeEvent.setText(PostData.getWaktu());
            picture=PostData.getPicture();
            latitude.setText(sharedpreferences.getString("latitude",null));
            longitude.setText(sharedpreferences.getString("longitude",null));
//            SharedPreferences.Editor editor1= sharedpreferences.edit();
            editor.remove("latitude");
            editor.remove("longitude");
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_post, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void selectImage(View v){
        final CharSequence[] items = { "Take Photo", "Choose from Library", "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(PostActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    mode = 1;
                    captureImage();
                }
                else if (items[item].equals("Choose from Library")) {
                    mode = 2;
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("image/*");
                    startActivityForResult(intent, media_code);
                }
                else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    //Select Picture
    @Override
    protected void onActivityResult(int reqCode, int resCode, Intent data) {
        if (mode == 1){

                if (resCode == RESULT_OK){
                    System.out.println("Masuk sini lho");
                    previewCapturedImage();
                }
                else if (resCode == RESULT_CANCELED){
                    Toast.makeText(getApplicationContext(), "" + "gagal mengambil foto", Toast.LENGTH_LONG).show();
                }
                else{
                    Toast.makeText(getApplicationContext(),""+"gagal mengambil foto", Toast.LENGTH_LONG).show();
                }

        }
        else if (mode == 2){
            if(resCode == Activity.RESULT_OK && data != null){
                // SDK < API11
                if (Build.VERSION.SDK_INT < 11){
                    realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(this, data.getData());
                    Uri uriFromPath = Uri.fromFile(new File(realPath));
                    imgEvent.setImageURI(uriFromPath);}
                // SDK >= 11 && SDK < 19
                else if (Build.VERSION.SDK_INT < 19){
                    realPath = RealPathUtil.getRealPathFromURI_API11to18(this, data.getData());
                    Uri uriFromPath = Uri.fromFile(new File(realPath));
                    imgEvent.setImageURI(uriFromPath);}
                // SDK > 19 (Android 4.4)
                else{
                    realPath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());
                    Uri uriFromPath = Uri.fromFile(new File(realPath));
                    imgEvent.setImageURI(uriFromPath);

                }
            }
        }
    }
    //#Select Picture

    //Camera

    private void captureImage(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri = getOutputMediaFileUri(media_foto);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent,foto_code);
    }

    public Uri getOutputMediaFileUri(int type){
        System.out.println(Uri.fromFile(getOutputMediaFile(type)));
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private File getOutputMediaFile(int type){
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()+"/ilate");
        mediaStorageDir.mkdirs();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type==media_foto){
            mediaFile = new File(mediaStorageDir.getPath()+File.separator+"IMG_"+timeStamp+".jpg");
            System.out.print(mediaStorageDir.getPath()+File.separator+"IMG_"+timeStamp+".jpg");
            PostActivity.this.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://"+mediaStorageDir)));
        }
        else {
            return null;
        }
        return mediaFile;
    }

    private void previewCapturedImage(){
        try{
            BitmapFactory.Options opt = new BitmapFactory.Options();
            opt.inSampleSize = 0;
            final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),opt);
//            Toast.makeText(this, fileUri.getPath(), Toast.LENGTH_LONG).show();
            realPath = fileUri.getPath();
            imgEvent.setImageBitmap(bitmap);
        }
        catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putParcelable("file_uri",fileUri);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);
        fileUri = savedInstanceState.getParcelable("file_uri");
    }

    //#Camera

    //Encode to base64
    public static String encodeTobase64(Bitmap image){
        System.gc();

        if (image == null){
            return null;
        }
        Bitmap imagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        imagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);

        byte[] b = baos.toByteArray();

        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        return imageEncoded;
    }

    public void preparingToUpload (View view){
        if (imgEvent.getDrawable()==null) {
            picture = "null";
        }
        else{
            Bitmap bitmap = ((BitmapDrawable) imgEvent.getDrawable()).getBitmap();
            picture = encodeTobase64(bitmap);
        }
//        upload(id, nameEvent.getText().toString(), deskripsiEvent.getText().toString(),
//                placeEvent.getText().toString(), timeEvent.getText().toString(),
//                latitude.getText().toString(), longitude.getText().toString(), picture);

        new TestPost(id, nameEvent.getText().toString(), deskripsiEvent.getText().toString(),
                placeEvent.getText().toString(), timeEvent.getText().toString(),
                latitude.getText().toString(), longitude.getText().toString(), picture).execute();

        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
        finish();
    }

    public void upload(int idUser, String judul_positng, String deskripsi_posting, String tempat, String waktu, String latitude, String longitude, String picture){
//        String idUserString = String.valueOf(idUser);
        try {
            DefaultHttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://192.168.137.1/progmob/index.php");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(9);
            nameValuePairs.add(new BasicNameValuePair("tag", "posting"));
            nameValuePairs.add(new BasicNameValuePair("idUser", String.valueOf(idUser)));
            nameValuePairs.add(new BasicNameValuePair("judulPost", judul_positng));
            nameValuePairs.add(new BasicNameValuePair("deskripsiPost", deskripsi_posting));
            nameValuePairs.add(new BasicNameValuePair("tempatPost", tempat));
            nameValuePairs.add(new BasicNameValuePair("timePost", waktu));
            nameValuePairs.add(new BasicNameValuePair("mapLatitude", latitude));
            nameValuePairs.add(new BasicNameValuePair("mapLongitude", longitude));
            nameValuePairs.add(new BasicNameValuePair("fotoPost", picture));

            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            HttpResponse response = httpclient.execute(httppost);
            String content = EntityUtils.toString(response.getEntity());
//            Log.e(,"post")

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private class TestPost extends AsyncTask<Void, Void, Void> {

        int id;
        String uploadJudul, uploadDeskripsi, uploadTempat, uploadWaktu, uploadLatitude, uploadLongitude, uploadPicture;

        public TestPost(int id, String judul, String deskripsi, String tempat, String waktu, String latitude, String longitude, String picture) {
            this.id = id;
            this.uploadJudul = judul;
            this.uploadDeskripsi = deskripsi;
            this.uploadTempat = tempat;
            this.uploadWaktu = waktu;
            this.uploadLatitude = latitude;
            this.uploadLongitude = longitude;
            this.uploadPicture = picture;
        }

        @Override
        protected Void doInBackground(Void... params) {
            upload(id, uploadJudul, uploadDeskripsi, uploadTempat, uploadWaktu, uploadLatitude, uploadLongitude, uploadPicture);

            return null;
        }

//        @Override
//        private Void onPostExecute(Void) {
//            startActivity(new Intent(getApplicationContext(), HomeActivity.class));
//            finish();
////            return null;
//        }


    }
}
