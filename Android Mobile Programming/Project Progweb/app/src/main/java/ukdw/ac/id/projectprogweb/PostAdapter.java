package ukdw.ac.id.projectprogweb;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Adhya Krisananta on 15/05/15.
 */
public class PostAdapter extends RecyclerView.Adapter<PostAdapter.PersonViewHolder> {

    ArrayList<Posts> myPosts;

    PostAdapter(ArrayList<Posts> myPosts) {
        this.myPosts = myPosts;
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_post, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PersonViewHolder personViewHolder, int i) {
        personViewHolder.judul_post.setText(myPosts.get(i).getJudul());
        personViewHolder.deskripsi_post.setText(myPosts.get(i).getDeskripsi());
//        personViewHolder.foto_user.setImageView;
        personViewHolder.nama_user.setText(myPosts.get(i).getId_user());


//        //new DownloadImageTask(imageView).execute(IMAGE_AVATAR + myPosts.get(position).getAvatar());
//        judul_post.setText(myPosts.get(i).getJudul());
//        deskripsi_post.setText(myPosts.get(i).getDeskripsi());
//        //kurang set image user
//        nama_user.setText(myPosts.get(i).getId_user());

    }

    @Override
    public int getItemCount() {
        return myPosts.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class PersonViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        //public View myView;
        CardView cv;

        TextView judul_post;
        TextView deskripsi_post;
        ImageView foto_user;
        TextView nama_user;

        public PersonViewHolder(View v) {
            super(v);
            cv = (CardView)itemView.findViewById(R.id.cv);
            //myView = v;
            judul_post = (TextView) v.findViewById(R.id.judulPost);
            deskripsi_post = (TextView) v.findViewById(R.id.deskripsiPost);
            // foto_user = (ImageView) v.findViewById(R.id.imgUser);
            nama_user = (TextView) v.findViewById(R.id.nameUser);
        }
    }
}
