package ukdw.ac.id.projectprogweb;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class HomeActivity extends ActionBarActivity {

    private SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;
    int idUser;
    String username;
    String namaUser;
    RecyclerView rv;

   @Override
    protected void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
       setContentView(R.layout.activity_home);

       sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
       SharedPreferences.Editor editor = sharedpreferences.edit();

       if (!sharedpreferences.contains("id")) {
           startActivity(new Intent(getApplicationContext(), MainActivity.class));
           finish();
       }
       idUser = sharedpreferences.getInt("id", 0);
       username = sharedpreferences.getString("username", null);
       namaUser = sharedpreferences.getString("nama", null);

       rv = (RecyclerView)findViewById(R.id.rv);
       rv.setHasFixedSize(true);

       LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
       rv.setLayoutManager(llm);

//       String a = sharedpreferences.getString("idPost", null);
//       Toast.makeText(this, a, Toast.LENGTH_LONG).show();

       new loadPost().execute();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_home, menu);
//        return true;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        else if(id==R.id.action_logout){
            logout();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            return true;
        }
//        else if(id==R.id.action_profile){
//            Intent intent = new Intent(this, ProfileActivity.class);
//            startActivity(intent);
//        }
//        else if(id==R.id.action_search){
//            Intent intent = new Intent(this, SearchActivity.class);
//            startActivity(intent);
//        }

        return super.onOptionsItemSelected(item);
    }

    public void post(View view){
        Intent intent = new Intent(this,PostActivity.class);
        startActivity(intent);

    }

    public void viewPost (View view){
        Intent intent = new Intent(this, ViewPostActivity.class);
        startActivity(intent);
    }

    public void logout(){
        SharedPreferences sharedPreferences = getSharedPreferences(MainActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear().commit();
        moveTaskToBack(true);
        HomeActivity.this.finish();
    }

    @Override
    public void onBackPressed(){
        finish();
    }

    public class loadPost extends AsyncTask<Void, Void, List<Posts>> {

        DefaultHttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://192.168.137.1/progmob/index.php");
        List<Posts> array = new ArrayList<>();

        @Override
        protected List<Posts> doInBackground(Void... params) {
            try {

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("tag", "loadPost"));
                //nameValuePairs.add(new BasicNameValuePair("id", idUser));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                HttpResponse httpResponse = httpclient.execute(httppost);
                HttpEntity httpEntity = httpResponse.getEntity();
                String data = EntityUtils.toString(httpEntity);

                JSONObject jObj = new JSONObject(data);
                JSONArray jsonArray = jObj.getJSONArray("posting");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jReal = jsonArray.getJSONObject(i);
                    array.add(new Posts(
                            jReal.getString("id_posting").toString(),
                            jReal.getString("username").toString(),
                            jReal.getString("judul_posting").toString(),
                            jReal.getString("diskripsi_posting").toString(),
                            jReal.getString("profilPicture").toString()
                            )
                    );
                }

                return array;

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(List<Posts> judul) {
            Context context = getApplicationContext();
            CardActivity adapter = new CardActivity(array, context, sharedpreferences);
            rv.setAdapter(adapter);

        }
    }
}
