package id.ac.ukdw.project4;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


public class MapActivity extends Activity implements LocationListener {

    private GoogleMap googleMap;
    LocationManager lm;
    Location location;
    Double latitudeC = 0.0;
    Double longitudeC = 0.0;
    MarkerOptions marker2;
    String nama="", deskripsi="";
    LatLng latlng;



    private static final long MIN_TIME = 400;
    private static final float MIN_DISTANCE = 1000;

    public static final String PREFS_NAME = "MyPref";
    static SharedPreferences map;
    SharedPreferences.Editor edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        map = getSharedPreferences(PREFS_NAME, 0);
        edit = map.edit();

        latitudeC = Double.parseDouble(map.getString("latitude", "0.0"));
        longitudeC = Double.parseDouble(map.getString("longitude", "0.0"));
        deskripsi = map.getString("deskripsi", "");
        nama = map.getString("name","");


        Toast.makeText(getApplicationContext(),nama + "\n" + latitudeC + "," + longitudeC, Toast.LENGTH_LONG).show();

        try {
            //Loading map
            initilizeMap();
        }
        catch (Exception e){
            e.printStackTrace();
        }

        //lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

        location = new Location("");
        location.setLatitude(latitudeC);
        location.setLongitude(longitudeC);

        onLocationChanged(location);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initilizeMap() {
        if (googleMap == null) {
            googleMap = ((MapFragment)getFragmentManager().findFragmentById(R.id.map)).getMap();
            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        latitudeC = location.getLatitude();
        longitudeC = location.getLongitude();
        latlng = new LatLng(latitudeC,longitudeC);
        if (marker2 == null){
           // marker2 = new MarkerOptions().position(new LatLng(latitudeC, longitudeC)).title("my location").snippet("posisi saya di lat : "+latitudeC+" long : "+longitudeC);
            //googleMap.addMarker(marker2);

            Marker marker1 = googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(latitudeC, longitudeC))
                    .title(nama)
                    .snippet("Koordinat : " + latitudeC + "," + longitudeC + "\n" + "Deskripsi : " + deskripsi));

            //when the location changes, update the map by zooming to the location
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latlng, 15);
            googleMap.animateCamera(cameraUpdate);

            marker1.showInfoWindow();

        }
        else{
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latlng, 15);
            googleMap.animateCamera(cameraUpdate);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
