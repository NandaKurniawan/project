package id.ac.ukdw.project4;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;


public class MainActivity extends Activity {

    Double latitude = 0.0, longtitude = 0.0;

    public static final String PREFS_NAME = "MyPref";
    static SharedPreferences map;
    SharedPreferences.Editor edit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        map = getSharedPreferences(PREFS_NAME, 0);
        edit = map.edit();

        LinearLayout linear1 = (LinearLayout)findViewById(R.id.LL1);
        LinearLayout linear2 = (LinearLayout)findViewById(R.id.LL2);
        LinearLayout linear3 = (LinearLayout)findViewById(R.id.LL3);
        LinearLayout linear4 = (LinearLayout)findViewById(R.id.LL4);
        LinearLayout linear5 = (LinearLayout)findViewById(R.id.LL5);

        linear1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit.clear().commit();
                edit.putString("latitude", "-7.785038");
                edit.putString("longitude", "110.379049");
                edit.putString("name", "Calzone Express");
                edit.putString("deskripsi","Pizzanya mantab !");
                edit.commit();

                Intent intent = new Intent(getApplicationContext(), MapActivity.class);
                startActivity(intent);
            }
        });

        linear2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit.clear().commit();
                edit.putString("latitude", "-7.782929");
                edit.putString("longitude", "110.385898");
                edit.putString("name", "Kedai IQ");
                edit.putString("deskripsi","Tempat nya asik !");
                edit.commit();

                Intent intent = new Intent(getApplicationContext(), MapActivity.class);
                startActivity(intent);
            }
        });

        linear3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit.clear().commit();
                edit.putString("latitude", "-7.77006");
                edit.putString("longitude", "110.390569");
                edit.putString("name", "Dixie Easy Dining");
                edit.putString("deskripsi","Makanannya mantab !");
                edit.commit();

                Intent intent = new Intent(getApplicationContext(), MapActivity.class);
                startActivity(intent);
            }
        });

        linear4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit.clear().commit();
                edit.putString("latitude", "-7.782852");
                edit.putString("longitude", "110.397989");
                edit.putString("name", "Rumah Makan Suharti");
                edit.putString("deskripsi","Ayamnya joss !");
                edit.commit();

                Intent intent = new Intent(getApplicationContext(), MapActivity.class);
                startActivity(intent);
            }
        });

        linear5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit.clear().commit();
                edit.putString("latitude", "-7.783343");
                edit.putString("longitude", "110.35007");
                edit.putString("name", "Gubug Makan Mang Engking");
                edit.putString("deskripsi","Udang + Ikan mantab abiss !");
                edit.commit();

                Intent intent = new Intent(getApplicationContext(), MapActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
