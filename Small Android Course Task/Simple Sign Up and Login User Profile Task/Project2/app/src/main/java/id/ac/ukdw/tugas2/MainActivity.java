package id.ac.ukdw.tugas2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {

    DBHelper mydb;

    private Button btnLogin, btnSignUp;
    private EditText txtUsername, txtPassword;
    private String username, password;
    //SharedPreferences sharedpreferences = getSharedPreferences("MyPrefs", 0);
    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String pass = "passwordKey";
    public static final String user = "userKey";
    public static final String id = "userId";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnLogin = (Button)findViewById(R.id.btnLogin);
        btnSignUp = (Button)findViewById(R.id.btnSignUp);
        txtUsername = (EditText)findViewById(R.id.txtUsernameLogin);
        txtPassword = (EditText)findViewById(R.id.txtPasswordLogin);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mydb = new DBHelper(this);
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void login (View view){
        username=txtUsername.getText().toString();
        password=txtPassword.getText().toString();

        if (username.equals("") || password.equals("")) {
            String b = "Username or Password is empty";
            Context contextb = getApplicationContext();
            Toast toast = Toast.makeText(contextb, b, Toast.LENGTH_SHORT);
            toast.show();
        }
        else {

            boolean cek = mydb.login(username, password);

            if(cek) {
                SharedPreferences sharedpreferences = getSharedPreferences("MyPrefs", 0);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString(user, username);
                editor.putString(pass, password);
                editor.putInt(id, mydb.getIdUser(username));
                editor.commit();

                Intent intent = new Intent(this, ViewActivity.class);
                startActivity(intent);
                finish();
            } else {
                String a = "Wrong Username or Password";
                Context context = getApplicationContext();
                Toast toast = Toast.makeText(context, a, Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }

    public void signup (View view){
        Intent intent = new Intent(this, SignupActivity.class);
        startActivity(intent);
    }

    protected void onResume() {
        SharedPreferences sharedpreferences = getSharedPreferences("MyPrefs", 0);
        sharedpreferences=getSharedPreferences(MyPREFERENCES,
                Context.MODE_PRIVATE);
        if (sharedpreferences.contains(user))
        {
            if(sharedpreferences.contains(pass)){
                Intent i = new Intent(this, ViewActivity.class);
                startActivity(i);
                finish();
            }
        }
        super.onResume();
    }

}
