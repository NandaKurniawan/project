package id.ac.ukdw.tugas2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;


public class SignupActivity extends ActionBarActivity {
    private DBHelper mydb = new DBHelper(this);
    private Button btnMakeSignUp, btnMakeAddPictureSignUp;
    private EditText txtMakeFullName, txtMakePassword, txtMakeUsername, txtMakeRePassword;
    private ImageView MakeViewProfilePicture;
    private String realPath, fullname, username, password, repassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

            btnMakeSignUp = (Button)findViewById(R.id.btnMakeSignUp);
            btnMakeAddPictureSignUp = (Button)findViewById(R.id.btnMakeAddPictureSignUp);
            txtMakeFullName = (EditText)findViewById(R.id.txtMakeFullName);
            txtMakeUsername = (EditText)findViewById(R.id.txtMakeUsername);
            txtMakePassword = (EditText)findViewById(R.id.txtMakePassword);
            txtMakeRePassword = (EditText)findViewById(R.id.txtMakeRePassword);
            MakeViewProfilePicture = (ImageView)findViewById(R.id.imgMakeViewProfilePictureSignUp);
        }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_signup, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void signup(View view){

        fullname = txtMakeFullName.getText().toString();
        username = txtMakeUsername.getText().toString();
        password = txtMakePassword.getText().toString();
        repassword = txtMakeRePassword.getText().toString();

        if(fullname.equals("")||username.equals("")){
            String f = "Fullname or Username is empty";
            Context contextf = getApplicationContext();
            Toast toast = Toast.makeText(contextf, f, Toast.LENGTH_SHORT);
            toast.show();
        }
        else{
                if(password.equals(repassword)){
                    if(mydb.insertUser(fullname, username, realPath, password)){;
                    Toast toast = Toast.makeText(getApplicationContext(), "User successfully added", Toast.LENGTH_SHORT);
                    toast.show();}
                    finish();
                    //Toast toast = Toast.makeText(getApplicationContext(), password, Toast.LENGTH_SHORT);
                    //toast.show();
                }
                else{
                    String c = "Password and Re-Type Password don't match";
                    Context contextc = getApplicationContext();
                    Toast toast = Toast.makeText(contextc, c, Toast.LENGTH_SHORT);
                    toast.show();
                }
        }
    }

    public void selectPicture (View view) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, 0);
    }

    @Override
    protected void onActivityResult(int reqCode, int resCode, Intent data) {
        if(resCode == Activity.RESULT_OK && data != null){
            // SDK < API11
            if (Build.VERSION.SDK_INT < 11){
                realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(this, data.getData());
                Uri uriFromPath = Uri.fromFile(new File(realPath));
                MakeViewProfilePicture.setImageURI(uriFromPath);}
                // SDK >= 11 && SDK < 19
            else if (Build.VERSION.SDK_INT < 19){
                realPath = RealPathUtil.getRealPathFromURI_API11to18(this, data.getData());
                Uri uriFromPath = Uri.fromFile(new File(realPath));
                MakeViewProfilePicture.setImageURI(uriFromPath);}
                // SDK > 19 (Android 4.4)
            else{
                realPath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());
                Uri uriFromPath = Uri.fromFile(new File(realPath));
                MakeViewProfilePicture.setImageURI(uriFromPath);
            }
        }
    }



}
