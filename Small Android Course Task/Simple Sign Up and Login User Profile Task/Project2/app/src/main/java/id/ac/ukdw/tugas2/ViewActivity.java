package id.ac.ukdw.tugas2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;


public class ViewActivity extends ActionBarActivity {

    private DBHelper mydb = new DBHelper(this);

    private Button btnSave, btnChangePicture;
    private EditText txtChangeFullName, txtChangePassword, txtChangeUsername, txtChangeRePassword;
    private ImageView ChangeImage;
    private String realPath = "", fullname, username, password, repassword, backupprofpic;
    public User user = new User();
    private int iduser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        btnSave = (Button)findViewById(R.id.btnSave);
        btnChangePicture = (Button)findViewById(R.id.btnChangePicture);
        txtChangeFullName = (EditText)findViewById(R.id.txtChangeFullName);
        txtChangeUsername = (EditText)findViewById(R.id.txtChangeUsername);
        txtChangePassword = (EditText)findViewById(R.id.txtChangePassword);
        txtChangeRePassword = (EditText)findViewById(R.id.txtChangeRePassword);
        ChangeImage = (ImageView)findViewById(R.id.imgChange);

        SharedPreferences sharedpreferences = getSharedPreferences("MyPrefs", 0);

        iduser = sharedpreferences.getInt("userId",0);

        user = mydb.getUser(iduser);

        backupprofpic = user.getProfPic();

        btnSave.setVisibility(View.INVISIBLE);
        btnChangePicture.setClickable(false);
        Uri uriFromPath = Uri.fromFile(new File(user.getProfPic()));
        ChangeImage.setImageURI(uriFromPath);
        txtChangeFullName.setText(user.getFullName());
        txtChangeFullName.setFocusable(false);
        txtChangeFullName.setClickable(false);
        txtChangeUsername.setText(user.getUsername());
        txtChangeUsername.setFocusable(false);
        txtChangeUsername.setClickable(false);
        txtChangePassword.setText(user.getPassword());
        txtChangePassword.setFocusable(false);
        txtChangePassword.setClickable(false);
        txtChangeRePassword.setText(user.getPassword());
        txtChangeRePassword.setFocusable(false);
        txtChangeRePassword.setClickable(false);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/
        if (id == R.id.Edit_Contact){
            edit();
        }
        else if (id == R.id.Sign_Out){
            logout();
        }

        return super.onOptionsItemSelected(item);
    }

    public void logout(){
        SharedPreferences sharedpreferences = getSharedPreferences("MyPrefs", 0);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear().commit();
        finish();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

    }

    public void edit(){
        btnSave.setVisibility(View.VISIBLE);

        btnChangePicture.setEnabled(true);
        btnChangePicture.setClickable(true);

        txtChangeFullName.setEnabled(true);
        txtChangeFullName.setFocusableInTouchMode(true);
        txtChangeFullName.setClickable(true);

        txtChangeUsername.setEnabled(true);
        txtChangeUsername.setFocusableInTouchMode(true);
        txtChangeUsername.setClickable(true);

        txtChangePassword.setEnabled(true);
        txtChangePassword.setFocusableInTouchMode(true);
        txtChangePassword.setClickable(true);

        txtChangeRePassword.setEnabled(true);
        txtChangeRePassword.setFocusableInTouchMode(true);
        txtChangeRePassword.setClickable(true);
    }

    public void save(View view){
        fullname = txtChangeFullName.getText().toString();
        username = txtChangeUsername.getText().toString();
        password = txtChangePassword.getText().toString();
        repassword = txtChangeRePassword.getText().toString();

        if(password.equals(repassword)){
            if(realPath.equals("")){
                if(mydb.updateUser(iduser,fullname, username, backupprofpic, password)){
                    Toast toast = Toast.makeText(getApplicationContext(), "User successfully update", Toast.LENGTH_SHORT);
                    toast.show();
                    finish();
                    Intent intent = new Intent(this, ViewActivity.class);
                    startActivity(intent);
                   }
            }
            else{
                if(mydb.updateUser(iduser,fullname, username, realPath, password)){
                    Toast toast = Toast.makeText(getApplicationContext(), "User successfully update", Toast.LENGTH_SHORT);
                    toast.show();
                    finish();
                    Intent intent = new Intent(this, ViewActivity.class);
                    startActivity(intent);
                }
            }
        }
        else{
            String c = "Password and Re-Type Password don't match";
            Context contextc = getApplicationContext();
            Toast toast = Toast.makeText(contextc, c, Toast.LENGTH_SHORT);
            toast.show();
        }



    }

    public void selectPictureEdit (View view) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, 0);
    }

    @Override
    protected void onActivityResult(int reqCode, int resCode, Intent data) {
        if(resCode == Activity.RESULT_OK && data != null){
            // SDK < API11
            if (Build.VERSION.SDK_INT < 11){
                realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(this, data.getData());
                Uri uriFromPath = Uri.fromFile(new File(realPath));
                ChangeImage.setImageURI(uriFromPath);}
            // SDK >= 11 && SDK < 19
            else if (Build.VERSION.SDK_INT < 19){
                realPath = RealPathUtil.getRealPathFromURI_API11to18(this, data.getData());
                Uri uriFromPath = Uri.fromFile(new File(realPath));
                ChangeImage.setImageURI(uriFromPath);}
            // SDK > 19 (Android 4.4)
            else{
                realPath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());
                Uri uriFromPath = Uri.fromFile(new File(realPath));
                ChangeImage.setImageURI(uriFromPath);
            }
        }
    }

}
