package id.ac.ukdw.tugas2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import java.util.ArrayList;


/**
 * Created by SAMSUNG on 11/04/2015.
 */
public class DBHelper extends SQLiteOpenHelper{

    public static final String DATABASE_NAME = "MyDB.db";
    public static final String DBUSER_TABLE_NAME = "dbuser";
    public static final String DBUSER_COLUMN_ID = "id";
    public static final String DBUSER_COLUMN_FULLNAME = "fullname";
    public static final String DBUSER_COLUMN_USERNAME = "username";
    public static final String DBUSER_COLUMN_PROFPIC = "profpic";
    public static final String DBUSER_COLUMN_PASSWORD = "password";
    //User user = new User();

    public DBHelper (Context context){
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE dbuser " + "(id INTEGER PRIMARY KEY, fullname TEXT, username TEXT, profpic TEXT, password TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS dbuser");
        onCreate(db);
    }

    public boolean insertUser (String fullname, String username, String profpic, String password) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        // data yang akan dimasukan kedalam tabel
        contentValues.put("fullname", fullname);
        contentValues.put("username", username);
        contentValues.put("profpic", profpic);
        contentValues.put("password", password);

        db.insert("dbuser", null, contentValues);
        return true;
    }

    public boolean updateUser(Integer id, String fullname, String username, String profpic, String password) {
        SQLiteDatabase db = this.getWritableDatabase();
        // contoh UPDATE database dengan SYNTAX.
        db.execSQL("UPDATE dbuser SET fullname = '" + fullname + "',username = '" + username + "',profpic = '" + profpic + "',password = '" + password + "' WHERE id = '" + id + "'");
        return true;
    }

    public boolean login(String username, String password){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = null;
        c = db.rawQuery("SELECT * FROM dbuser WHERE username = '" + username + "'AND password = '" + password +"' ", null);
        c.moveToFirst();
        c.close();

        int i = c.getCount();
        if(i <= 0){
            return false;
        }
        else
            return true;
    }

    public User getUser(int id) {
        //ArrayList arrayList = new ArrayList();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM dbuser WHERE id = '" + id + "' ", null);

        // moveToFirst() digunakan untuk memindah cursor ke baris pertama.
        res.moveToFirst();
        User user = new User();
        // digunakan untuk cek apakah cursor berada pada baris terakhir.
        while (res.isAfterLast() == false) {
            // memasukan hasil yang ditunjuk oleh cursor ke object contact.
          // User user = new User();
            user.setId(res.getInt(res.getColumnIndex(DBUSER_COLUMN_ID)));
            user.setFullName(res.getString(res.getColumnIndex(DBUSER_COLUMN_FULLNAME)));
            user.setUsername(res.getString(res.getColumnIndex(DBUSER_COLUMN_USERNAME)));
            user.setProfPic(res.getString(res.getColumnIndex(DBUSER_COLUMN_PROFPIC)));
            user.setPassword(res.getString(res.getColumnIndex(DBUSER_COLUMN_PASSWORD)));
            // memasukan object contact ke arraylist.
            //arrayList.add(user) ;
            // memindah cursor ke baris berikutnya.
            res.moveToNext();
        }
        return user;
    }

    public int getIdUser(String username){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT id FROM dbuser WHERE username = '" + username + "' ", null);
        res.moveToFirst();
        return res.getInt(res.getColumnIndex("id"));
    }



}
