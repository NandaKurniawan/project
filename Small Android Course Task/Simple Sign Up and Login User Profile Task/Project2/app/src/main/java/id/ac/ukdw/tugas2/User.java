package id.ac.ukdw.tugas2;

/**
 * Created by SAMSUNG on 11/04/2015.
 */
public class User {
    int id;
    String fullname, username, profpic, password;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this. id = id;
    }

    public String getFullName() {
        return fullname;
    }
    public void setFullName(String fullname) {
        this. fullname = fullname;
    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this. username = username;
    }

    public String getProfPic() {
        return profpic;
    }
    public void setProfPic(String profpic) {this. profpic = profpic;}

    public String getPassword() {return password;}
    public void setPassword(String password) {this. password = password;}

    @Override
    public String toString(){
        return this. fullname;
    }
}
