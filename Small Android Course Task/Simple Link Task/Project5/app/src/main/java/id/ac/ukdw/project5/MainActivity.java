package id.ac.ukdw.project5;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends Activity {
    private String URL="", password="";
    private Button buttonLogin;
    private EditText textURL, textPassword;
    SharedPreferences share;
    SharedPreferences.Editor edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        share = getSharedPreferences("MyPref", 0);

        textURL = (EditText)findViewById(R.id.txtURL);
        textPassword = (EditText)findViewById(R.id.txtPassword);

        buttonLogin = (Button)findViewById(R.id.btnLogin);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                URL = textURL.getText().toString();
                password = textPassword.getText().toString();

                if(URL.equals("")||password.equals("")){
                    Toast.makeText(getApplicationContext(),"Password or Link Facebook is empty !",Toast.LENGTH_SHORT).show();
                }
                else if (!password.equals("12345")){
                    Toast.makeText(getApplicationContext(),"Password is wrong !",Toast.LENGTH_SHORT).show();
                }
                else{
                    edit = share.edit();
                    edit.clear().commit();
                    edit.putString("link", URL);
                    edit.commit();

                    Intent intent = new Intent(getApplicationContext(), ViewActivity.class);
                    startActivity(intent);
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
