package id.ac.ukdw.project5;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class ViewActivity extends Activity {

    private String id, first_name, gender, last_name, link, locale, name, username, middlename="";
    private TextView textId, textFirst_Name, textGender, textLast_Name, textLink, textLocale, textName, textUsername, textMiddlename;
    private ImageView photo;
    private Bitmap bmp;
    LinearLayout linearmiddlename;
    SharedPreferences share;
    SharedPreferences.Editor edit;

    //URL untuk mengakses JSON
    //private static String baseURL = "https://graph.facebook.com/";

    //JSON NOde Names
    private static final String TAG_ID = "id";
    private static final String TAG_FIRST_NAME = "first_name";
    private static final String TAG_GENDER = "gender";
    private static final String TAG_LAST_NAME = "last_name";
    private static final String TAG_LINK = "link";
    private static final String TAG_LOCALE = "locale";
    private static final String TAG_MIDDLE_NAME = "middle_name";
    private static final String TAG_NAME = "name";
    private static final String TAG_USERNAME = "username";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);
        share = getSharedPreferences("MyPref", 0);

        //String facebook = share.getString("link","");

       //baseURL = baseURL + share.getString("link","");

        Toast.makeText(getApplicationContext(),share.getString("link",""), Toast.LENGTH_LONG).show();
        //middlename="";
        new JSONParse().execute();

    }

    private class JSONParse extends AsyncTask<String, String, JSONObject> {
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            //sebelum mulai jalanin thread kita mau ngapain
            super.onPreExecute();
            textId = (TextView) findViewById(R.id.txtId);
            textFirst_Name = (TextView) findViewById(R.id.txtFirstName);
            textGender = (TextView) findViewById(R.id.txtGender);
            textLast_Name = (TextView) findViewById(R.id.txtLastName);
            textLink = (TextView) findViewById(R.id.txtLink);
            textLocale = (TextView) findViewById(R.id.txtLocale);
            textName = (TextView) findViewById(R.id.txtName);
            textUsername = (TextView) findViewById(R.id.txtUsername);
            photo = (ImageView) findViewById(R.id.imgPP);
            textMiddlename = (TextView) findViewById(R.id.txtMiddleName);
            linearmiddlename = (LinearLayout)findViewById(R.id.linearmiddle);

            pDialog = new ProgressDialog(ViewActivity.this);
            pDialog.setMessage("Getting Data ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();

            linearmiddlename.setVisibility(View.VISIBLE);
        }

        @Override
        protected JSONObject doInBackground(String... args) {
            //yang akan di lakukan di background
            //String..., mksud dari 3 titik yaitu karena dianggao sebagai array
            JSONObject jsonObject = null;
            StringBuilder builder = new StringBuilder();
            HttpClient client = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(share.getString("link",""));

            try {
                //Mencoba mengakses Web Services
                HttpResponse response = client.execute(httpGet);
                //Mendapatkan status keberadaan Web Services
                StatusLine statusLine = response.getStatusLine();
                int statusCode = statusLine.getStatusCode();
                //Jika Web Services Tersedia
                if (statusCode == 200)
                {
                    HttpEntity entity = response.getEntity();
                    InputStream content = entity.getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        builder.append(line);
                    }
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                jsonObject = new JSONObject(builder.toString());
                // Menyimpan item data dalam JSON dalam variabel
                id = jsonObject.getString(TAG_ID);
                first_name = jsonObject.getString(TAG_FIRST_NAME);
                gender = jsonObject.getString(TAG_GENDER);
                last_name = jsonObject.getString(TAG_LAST_NAME);
                link = jsonObject.getString(TAG_LINK);
                locale = jsonObject.getString(TAG_LOCALE);
                name = jsonObject.getString(TAG_NAME);
                username = jsonObject.getString(TAG_USERNAME);
                middlename = jsonObject.getString(TAG_MIDDLE_NAME);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return jsonObject;
        }
        @Override
        protected void onPostExecute(JSONObject json) {
            //menerima hasil dari doInBackground
            new ProfPic(share.getString("link","")+"/picture?type=large", photo).execute();
            //pDialog.dismiss();
            textId.setText(": " + id);
            textFirst_Name.setText(": " +first_name);
            textGender.setText(": " + gender);
            textLast_Name.setText(": "+last_name);
            textLink.setText(": "+link);
            textLocale.setText(": "+locale);
            textName.setText(": "+name);

            if(middlename.equals("")){
                linearmiddlename.setVisibility(View.GONE);
            }
            else{
                textMiddlename.setText(": "+middlename);
            }
            textUsername.setText(": " + username);
            middlename = "";
            pDialog.dismiss();
        }
    }

    private class ProfPic extends AsyncTask<Void, Void, Bitmap>{

        private String URL;
        private ImageView imgProf;

        public ProfPic(String URL, ImageView imgProf) {
            this.URL = URL;
            this.imgProf = imgProf;
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            try {
                URL urlConnection = new URL(URL);
                HttpURLConnection conn = (HttpURLConnection) urlConnection.openConnection();
                conn.setDoInput(true);
                conn.connect();
                InputStream input = conn.getInputStream();
                Bitmap bitmap = BitmapFactory.decodeStream(input);
                return bitmap;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            imgProf.setImageBitmap(result);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
