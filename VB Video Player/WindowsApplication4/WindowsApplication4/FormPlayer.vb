﻿Imports AxWMPLib
Imports WMPLib
Imports System.IO

Public Class FormPlayer
    Private fullscreen = False
    Private reader As StreamReader
    Private leaveCount As Integer = 0
    Private FromTime As List(Of TimeSpan) = New List(Of TimeSpan)
    Private ToTime As List(Of TimeSpan) = New List(Of TimeSpan)
    Private Subs As List(Of String) = New List(Of String)
    Private timeElapsed As TimeSpan
    Public currentIndex As Integer
    Public subsIndex As Integer
    Public SongPath As String
    Dim showsubs As Boolean = False
    Dim cekScroll As TimeSpan = New TimeSpan(0, 0, 0, 5)
    Dim compare As TimeSpan = New TimeSpan(0, 0, 0, 0, 400)
    Dim controlHide = 419
    Dim controlsShow = True

    Public Function play()
        If (Player.playState.Equals(WMPLib.WMPPlayState.wmppsPaused)) Then
            If File.Exists(SongPath.Substring(0, SongPath.Length - 4) + ".srt") Then
                TimerSubs.Enabled = True
            End If
            Player.Ctlcontrols.play()
        ElseIf SongPath <> Nothing Then
            If File.Exists(SongPath.Substring(0, SongPath.Length - 4) + ".srt") Then
                reader = New StreamReader(SongPath.Substring(0, SongPath.Length - 4) + ".srt")
                Dim temp As String
                Dim temp2 As String
                While reader.EndOfStream = False
                    reader.ReadLine()
                    temp = reader.ReadLine()
                    FromTime.Add(TimeSpan.Parse(temp.Substring(0, 12)))
                    ToTime.Add(TimeSpan.Parse(temp.Substring(17, 12)))
                    temp = reader.ReadLine()
                    While True
                        temp2 = reader.ReadLine()
                        If temp2 = "" Then
                            Exit While
                        Else
                            temp += vbCrLf + temp2
                        End If
                    End While
                    Subs.Add(temp)
                End While
                subsIndex = 0
                Subtitle.Show()
                Subtitle.lblSubs.Visible = False
            End If
            Player.URL = SongPath
            Dim ext As String = SongPath.Substring((SongPath.Length - 3), 3).ToString.ToLower
            If (ext = "avi" Or ext = "mp4" Or ext = "wmv" Or ext = "mov" Or ext = "mp3" Or ext = "wav" Or ext = "m4a" Or ext = "wma") Then
                If (ext = "avi" Or ext = "mp4" Or ext = "wmv" Or ext = "mov") Then
                    btnFullscreen.PerformClick()
                End If
                If File.Exists(SongPath.Substring(0, SongPath.Length - 4) + ".srt") Then
                    TimerSubs.Start()
                End If
                Player.Ctlcontrols.play()
                Player.stretchToFit = True
                FormPlaylist.ListBoxSong.SelectedIndex = currentIndex
                ToolStripStatusSongPath.Text = FormPlaylist.ListBoxSong.SelectedItem.ToString()
                TimerPlay.Start()
            Else
                MessageBox.Show("Media Format not supported!", "Extension Error")
            End If
        End If
    End Function

    Public Function nxt()
        Dim index As Integer = currentIndex
        If (FormPlaylist.ListBoxSong.Items.Count <> 0) Then
            If (index + 1 <= FormPlaylist.ListBoxSong.Items.Count - 1) Then
                FormPlaylist.ListBoxSong.SetSelected(index + 1, True)
                FormPlaylist.ListBoxSong.SetSelected(index, False)
            Else
                FormPlaylist.ListBoxSong.SetSelected(0, True)
                FormPlaylist.ListBoxSong.SetSelected(index, False)
            End If
            play()
        End If
    End Function

    Public Function prev()
        Dim index As Integer = currentIndex
        If (FormPlaylist.ListBoxSong.Items.Count <> 0) Then
            If (index - 1 >= 0) Then
                FormPlaylist.ListBoxSong.SetSelected(index - 1, True)
                FormPlaylist.ListBoxSong.SetSelected(index, False)
            Else
                FormPlaylist.ListBoxSong.SetSelected(FormPlaylist.ListBoxSong.Items.Count - 1, True)
                FormPlaylist.ListBoxSong.SetSelected(index, False)
            End If
            play()
        End If
    End Function

    Public Function rand()
        If (FormPlaylist.ListBoxSong.Items.Count <> 0) Then
            If FormPlaylist.ListBoxSong.SelectedIndices.Count > 0 Then
                FormPlaylist.ListBoxSong.SetSelected(currentIndex, False)
            End If
            FormPlaylist.ListBoxSong.SetSelected(CInt(Rnd() * (FormPlaylist.ListBoxSong.Items.Count - 1)), True)
            play()
        End If
    End Function

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerPlay.Tick
        If (Player.playState = WMPLib.WMPPlayState.wmppsStopped) Then
            TimerPlay.Stop()
            TimerSubs.Stop()
            If (FormPlaylist.Repeat = True) Then
                play()
            ElseIf (FormPlaylist.Random = True) Then
                rand()
            ElseIf (FormPlaylist.ListBoxSong.SelectedIndex = FormPlaylist.ListBoxSong.Items.Count - 1) Then
                Player.Ctlcontrols.stop()
                lblTimeElapsed.Text = "00:00"
                TrackBarProgress.Value = 0
            Else
                nxt()
            End If
        Else
            lblDuration.Text = Player.currentMedia.durationString
            TrackBarProgress.Minimum = 0
            TrackBarProgress.Maximum = Player.currentMedia.duration
            lblTimeElapsed.Text = Player.Ctlcontrols.currentPositionString
            If (Player.Ctlcontrols.currentPosition <= Player.currentMedia.duration) Then
                TrackBarProgress.Value = Player.Ctlcontrols.currentPosition
            End If
        End If
    End Sub

    Private Sub TrackBarProgress_Scroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TrackBarProgress.Scroll
        Player.Ctlcontrols.currentPosition = TrackBarProgress.Value
        timeElapsed = TimeSpan.Parse(Player.Ctlcontrols.currentPositionString)
    End Sub

    Private Sub FormPlayer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormPlaylist.Visible = False
        Player.uiMode = "none"
        Subtitle.lblSubs.Visible = False
        Subtitle.TopMost = True
    End Sub

    Private Sub btnPlaylist_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPlaylist.Click
        If (FormPlaylist.Visible = False) Then
            FormPlaylist.Visible = True
            btnPlaylist.BackColor = Color.DeepSkyBlue
            btnPlaylist.ForeColor = Color.Black
        Else
            FormPlaylist.Visible = False
            btnPlaylist.UseVisualStyleBackColor = True
            btnPlaylist.ForeColor = Color.DeepSkyBlue
        End If
    End Sub

    Private Sub btnPlay_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPlay.Click
        If (Player.playState.Equals(WMPLib.WMPPlayState.wmppsPlaying)) Then
            Player.Ctlcontrols.pause()
            TimerSubs.Enabled = False
        Else
            play()
        End If
    End Sub

    Private Sub btnPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrev.Click
        If (FormPlaylist.Random = True) Then
            rand()
        Else
            prev()
        End If
    End Sub

    Private Sub btnStop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStop.Click
        Player.Ctlcontrols.stop()
        TimerPlay.Stop()
        TimerSubs.Stop()
        lblTimeElapsed.Text = "00:00"
        TrackBarProgress.Value = 0
    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        If (FormPlaylist.Random = True) Then
            rand()
        Else
            nxt()
        End If
    End Sub

    Private Sub TrackBarVolume_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TrackBarVolume.ValueChanged
        If Player.IsHandleCreated.Equals(True) Then
            Player.settings.volume = TrackBarVolume.Value
        End If
    End Sub

    Private Sub Player_PlayStateChange(ByVal sender As System.Object, ByVal e As AxWMPLib._WMPOCXEvents_PlayStateChangeEvent) Handles Player.PlayStateChange
        If (Player.playState.Equals(WMPLib.WMPPlayState.wmppsPaused)) Then
            TimerSubs.Enabled = False
            btnPlay.Text = "Play"
            btnPlay.UseVisualStyleBackColor = True
            btnPlay.ForeColor = Color.DeepSkyBlue
        ElseIf (Player.playState.Equals(WMPLib.WMPPlayState.wmppsPlaying)) Then
            btnPlay.Text = "Pause"
            btnPlay.BackColor = Color.DeepSkyBlue
            btnPlay.ForeColor = Color.Black
        Else
            btnPlay.Text = "Play"
            btnPlay.UseVisualStyleBackColor = True
            btnPlay.ForeColor = Color.DeepSkyBlue
        End If
    End Sub

    Private Sub TimerShow_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerShow.Tick
        If controlHide - PanelControl.Bounds.Location.Y < 90 Then
            PanelControl.SetBounds(PanelControl.Bounds.X, PanelControl.Bounds.Y - 5, PanelControl.Width, PanelControl.Height)
            If (Subtitle.lblSubs.Visible = True) Then
                Subtitle.lblSubs.Visible = False
            End If
        ElseIf controlHide - PanelControl.Bounds.Location.Y >= 90 Then
            TimerShow.Enabled = False
            PanelControl.Focus()
            controlsShow = True
        End If
    End Sub

    Private Sub TimerHide_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerHide.Tick
        If controlHide - PanelControl.Bounds.Location.Y > 5 Then
            PanelControl.SetBounds(PanelControl.Bounds.X, PanelControl.Bounds.Y + 5, PanelControl.Width, PanelControl.Height)
        ElseIf controlHide - PanelControl.Bounds.Location.Y <= 5 Then
            TimerHide.Enabled = False
            controlsShow = False
            If (Subtitle.lblSubs.Visible = False And showsubs = True) Then
                Subtitle.lblSubs.Visible = True
            End If
        End If
    End Sub

    Private Sub PanelControl_MouseEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PanelControl.MouseEnter
        If TimerHide.Enabled = False Then
            TimerShow.Enabled = True
        End If
    End Sub

    Private Sub TimerLeave_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerLeave.Tick
        leaveCount += 1
        If (leaveCount >= 4) And TimerShow.Enabled = False Then
            TimerLeave.Enabled = False
            leaveCount = 0
            TimerHide.Enabled = True
        ElseIf (leaveCount >= 2) And TimerShow.Enabled = False Then
            TimerLeave.Enabled = False
            leaveCount = 0
            TimerHide.Enabled = True
        End If
    End Sub

    Private Sub FormPlayer_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.C Then
            Dim lctn As System.Drawing.Point
            lctn = Me.Player.PointToScreen(New Point(0, 0))
            Dim bmp As Bitmap = New Bitmap(Me.Player.Bounds.Width, Me.Player.Bounds.Height, Imaging.PixelFormat.Format32bppArgb)
            Dim gfx As Graphics = Graphics.FromImage(bmp)
            gfx.CopyFromScreen(lctn.X, lctn.Y, 0, 0, Me.Player.Size, CopyPixelOperation.SourceCopy)
            SaveFileDialog1.Filter = "Image Files (*.bmp, *.jpg, *.png)|*.bmp;*.jpg;*.png"
            If SaveFileDialog1.ShowDialog = DialogResult.OK Then
                If SaveFileDialog1.CheckPathExists = True Then
                    Dim path As String = SaveFileDialog1.FileName
                    bmp.Save(path, Imaging.ImageFormat.Jpeg)
                End If
            End If
        ElseIf e.KeyCode = Keys.Space Then
            btnPlay.PerformClick()
        End If
    End Sub

    Private Sub Player_KeyDownEvent(ByVal sender As System.Object, ByVal e As AxWMPLib._WMPOCXEvents_KeyDownEvent) Handles Player.KeyDownEvent
        If e.nKeyCode = Keys.C And Player.fullScreen = True Then
            Dim bmp As Bitmap = New Bitmap(My.Computer.Screen.Bounds.Width, My.Computer.Screen.Bounds.Height, Imaging.PixelFormat.Format32bppArgb)
            Dim gfx As Graphics = Graphics.FromImage(bmp)
            gfx.CopyFromScreen(0, 0, 0, 0, My.Computer.Screen.Bounds.Size, CopyPixelOperation.SourceCopy)
            SaveFileDialog1.Filter = "Image Files (*.bmp, *.jpg, *.png)|*.bmp;*.jpg;*.png"
            If SaveFileDialog1.ShowDialog = DialogResult.OK Then
                If SaveFileDialog1.CheckPathExists = True Then
                    Dim path As String = SaveFileDialog1.FileName
                    bmp.Save(path, Imaging.ImageFormat.Jpeg)
                End If
            End If
        ElseIf e.nKeyCode = Keys.C Then
            FormPlayer_KeyDown(Nothing, New KeyEventArgs(Keys.C))
        ElseIf e.nKeyCode = Keys.Space Then
            btnPlay.PerformClick()
        End If
    End Sub

    Private Sub TimerSubs_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerSubs.Tick
        'Masih buggy pak, proses develop supaya support fast forward subtitlenya :) 
        timeElapsed = timeElapsed.Add(TimeSpan.FromMilliseconds(200))
        If (FromTime(subsIndex).Subtract(timeElapsed) <= compare And showsubs = False) Then
            showsubs = True
            Subtitle.lblSubs.Text = Subs(subsIndex)
            If (controlsShow = False) Then
                Subtitle.lblSubs.Visible = True
            End If
        ElseIf (ToTime(subsIndex).Subtract(timeElapsed) <= compare And showsubs = True) Then
            Subtitle.lblSubs.Visible = False
            showsubs = False
            subsIndex += 1
        End If
    End Sub

    Private Sub FormPlayer_Resize(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Resize
        If Me.Width <= 587 Then
            TrackBarVolume.Visible = False
        Else
            TrackBarVolume.Visible = True
        End If
    End Sub

    Private Sub PanelControl_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PanelControl.Enter
        TimerLeave.Enabled = True
    End Sub

    Private Sub btnFullscreen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFullscreen.Click
        If (fullscreen = False) Then
            If (FormPlaylist.Visible = True) Then
                btnPlaylist.PerformClick()
            End If
            btnFullscreen.Text = "Restore"
            btnFullscreen.BackColor = Color.DeepSkyBlue
            btnFullscreen.ForeColor = Color.Black
            Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None
            Me.SetBounds(0, 0, My.Computer.Screen.Bounds.Width, My.Computer.Screen.Bounds.Height)
            Player.stretchToFit = True
            Subtitle.SetBounds(0, Me.Location.Y + 600, Subtitle.Width, Subtitle.Height)
            StatusStrip1.Visible = False
            controlHide = StatusStrip1.Bounds.Y + 22
            fullscreen = True
        ElseIf (fullscreen = True) Then
            btnFullscreen.Text = "Full Screen"
            btnFullscreen.UseVisualStyleBackColor = True
            btnFullscreen.ForeColor = Color.DeepSkyBlue
            Me.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
            Player.stretchToFit = False
            Me.SetBounds(363, 129, 640, 480)
            Subtitle.SetBounds(Me.Location.X + 7, Me.Location.Y + 371, Subtitle.Width, Subtitle.Height)
            StatusStrip1.Visible = True
            controlHide = StatusStrip1.Bounds.Y
            fullscreen = False
        End If
    End Sub

    Private Sub FormPlayer_Move(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Move
        Subtitle.SetBounds(Me.Location.X + 7, Me.Location.Y + 371, Subtitle.Width, Subtitle.Height)
    End Sub

    Private Sub Player_SizeChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Player.SizeChanged
        Subtitle.Width = Player.Width
        Subtitle.Height = CInt(Player.Height / 5)
    End Sub

    Private Sub PanelControl_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PanelControl.Leave
        TimerLeave.Enabled = True
    End Sub

    Private Sub lblDuration_SizeChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblDuration.SizeChanged
        If lblDuration.Size.Width > 34 Then
            lblDuration.SetBounds(lblDuration.Bounds.X - 15, lblDuration.Bounds.Y, lblDuration.Width, lblDuration.Height)
        Else
            lblDuration.SetBounds(572, lblDuration.Bounds.Y, lblDuration.Width, lblDuration.Height)
        End If
    End Sub

    Private Sub FormPlayer_SizeChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.SizeChanged
        Subtitle.SetBounds(Me.Location.X + 7, Me.Location.Y + 371, Me.Player.Size.Width, CInt(Me.Player.Size.Height / 5))
        Subtitle.lblSubs.SetBounds(CInt((Subtitle.Width / 2) - (Subtitle.lblSubs.Width / 2)), Subtitle.lblSubs.Location.Y, Subtitle.lblSubs.Width, Subtitle.lblSubs.Height)
        controlHide = StatusStrip1.Bounds.Y
    End Sub

    Private Sub Player_DoubleClickEvent(ByVal sender As System.Object, ByVal e As AxWMPLib._WMPOCXEvents_DoubleClickEvent) Handles Player.DoubleClickEvent
        btnFullscreen.PerformClick()
    End Sub

    Private Sub blue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles blue.Click
        Subtitle.lblSubs.ForeColor = Color.Blue
    End Sub

    Private Sub green_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles green.Click
        Subtitle.lblSubs.ForeColor = Color.Green
    End Sub

    Private Sub yellow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles yellow.Click
        Subtitle.lblSubs.ForeColor = Color.Yellow
    End Sub

    Private Sub cyan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cyan.Click
        Subtitle.lblSubs.ForeColor = Color.Cyan
    End Sub

    Private Sub WhiteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WhiteToolStripMenuItem.Click
        Subtitle.lblSubs.ForeColor = Color.White
    End Sub

    Private Sub BiggerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BiggerToolStripMenuItem.Click
        'Subtitle.lblSubs.Size.Add(
        'Subtitle.lblSubs.Height += 1
    End Sub

    Private Sub SmallerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SmallerToolStripMenuItem.Click
        'Subtitle.lblSubs.Width -= 1
        'Subtitle.lblSubs.Height -= 1
    End Sub

End Class
