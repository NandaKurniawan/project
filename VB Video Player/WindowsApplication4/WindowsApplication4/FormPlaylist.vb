﻿Imports WMPLib
Imports System.IO

Public Class FormPlaylist
    Public FolderPath As String
    Public Random As Boolean = False
    Public Repeat As Boolean = False

    Private Sub btnOpenDir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenDir.Click
        If FolderBrowserDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            FolderPath = FolderBrowserDialog1.SelectedPath
            Dim folderInfo As New IO.DirectoryInfo(FolderPath)
            ListBoxSong.Items.Clear()
            ListBoxSong.Items.AddRange(folderInfo.GetFiles("*.mp3"))
            ListBoxSong.Items.AddRange(folderInfo.GetFiles("*.mp4"))
            ListBoxSong.Items.AddRange(folderInfo.GetFiles("*.wav"))
            ListBoxSong.Items.AddRange(folderInfo.GetFiles("*.m4a"))
            ListBoxSong.Items.AddRange(folderInfo.GetFiles("*.avi"))
            ListBoxSong.Items.AddRange(folderInfo.GetFiles("*.wmv"))
            ListBoxSong.Items.AddRange(folderInfo.GetFiles("*.wma"))
            ListBoxSong.Items.AddRange(folderInfo.GetFiles("*.mov"))
            ListBoxSong.SelectedIndex = 0
        End If
    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        Dim index As Integer
        Do
            If ListBoxSong.SelectedIndices.Count > 1 Then
                For Each index In ListBoxSong.SelectedIndices
                    ListBoxSong.Items.RemoveAt(index)
                Next
            End If
            If ListBoxSong.SelectedIndices.Count = 1 Then
                index = ListBoxSong.SelectedIndex
                If index = ListBoxSong.Items.Count - 1 Then
                    ListBoxSong.SelectedIndex = index - 1
                Else
                    ListBoxSong.SelectedIndex = index + 1
                End If
                ListBoxSong.Items.RemoveAt(index)
            End If
        Loop While ListBoxSong.SelectedIndices.Count <> 1
    End Sub

    Private Sub btnRandom_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRandom.Click
        If (Random = False) Then
            Random = True
            btnRandom.BackColor = Color.DeepSkyBlue
            btnRandom.ForeColor = Color.Black
        Else
            Random = False
            btnRandom.UseVisualStyleBackColor = True
            btnRandom.ForeColor = Color.DeepSkyBlue
        End If
    End Sub

    Private Sub btnRepeat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRepeat.Click
        If (Repeat = False) Then
            Repeat = True
            btnRepeat.BackColor = Color.DeepSkyBlue
            btnRepeat.ForeColor = Color.Black
        Else
            Repeat = False
            btnRepeat.UseVisualStyleBackColor = True
            btnRepeat.ForeColor = Color.DeepSkyBlue
        End If
    End Sub

    Private Sub ListBoxSong_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBoxSong.SelectedIndexChanged
        If ListBoxSong.SelectedItems.Count >= 1 Then
            FormPlayer.currentIndex = ListBoxSong.SelectedIndex
            If ListBoxSong.SelectedItem.ToString.Contains("\") Then
                FormPlayer.SongPath = ListBoxSong.SelectedItem.ToString
            Else
                FormPlayer.SongPath = FolderPath + "\" + ListBoxSong.SelectedItem.ToString
            End If
        End If
    End Sub

    Private Sub btnAddFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddFile.Click
        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Dim file As String
            For Each file In OpenFileDialog1.FileNames
                ListBoxSong.Items.Add(file)
            Next
        End If
    End Sub

    Private Sub ListBoxSong_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBoxSong.DoubleClick
        FormPlayer.play()
    End Sub

    Private Sub FormPlaylist_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        e.Cancel = True
        Me.Visible = False
        FormPlayer.btnPlaylist.UseVisualStyleBackColor = True
        FormPlayer.btnPlaylist.ForeColor = Color.DeepSkyBlue
    End Sub

    Private Sub FormPlaylist_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.CreateControl()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then

            If Path.GetExtension(SaveFileDialog1.FileName) = ".m3u" Then
                'Dim isifile As List(Of String)
                Dim index As Integer = ListBoxSong.Items.Count
                Dim indexarray As Integer = 0
                Dim arrayisi(index * 2 + 1) As String
                'Dim arrayisi(indexarray) As String
                arrayisi(indexarray) = "#EXTM3U"
                indexarray = indexarray + 1

                If ListBoxSong.SelectedItem.ToString.Contains("\") Then
                    For a As Integer = 1 To index
                        Dim folderInfo As New IO.DirectoryInfo((ListBoxSong.Items.Item(a - 1)).ToString)
                        Dim mp As WindowsMediaPlayer = New WindowsMediaPlayer
                        Dim song As WMPLib.IWMPMedia = mp.newMedia((ListBoxSong.Items.Item(a - 1)).ToString)
                        arrayisi(indexarray) = "#EXTINF:" + song.durationString + "," + (ListBoxSong.Items.Item(a - 1)).ToString
                        indexarray = indexarray + 1
                        arrayisi(indexarray) = (ListBoxSong.Items.Item(a - 1)).ToString
                        indexarray = indexarray + 1
                    Next
                Else
                    For a As Integer = 1 To index
                        Dim folderInfo As New IO.DirectoryInfo((FolderPath + "\" + (ListBoxSong.Items.Item(a - 1)).ToString).ToString)
                        Dim mp As WindowsMediaPlayer = New WindowsMediaPlayer
                        Dim song As WMPLib.IWMPMedia = mp.newMedia((FolderPath + "\" + (ListBoxSong.Items.Item(a - 1)).ToString).ToString)
                        arrayisi(indexarray) = "#EXTINF:" + song.durationString + "," + (FolderPath + "\" + (ListBoxSong.Items.Item(a - 1)).ToString).ToString
                        indexarray = indexarray + 1
                        arrayisi(indexarray) = (FolderPath + "\" + (ListBoxSong.Items.Item(a - 1)).ToString).ToString
                        indexarray = indexarray + 1
                    Next
                    'FormPlayer.SongPath = FolderPath + "\" + ListBoxSong.SelectedItem.ToString
                End If

                Dim b As Integer = indexarray
                Dim astring As String = ""
                For indexarray = 0 To b
                    astring = astring + arrayisi(indexarray) + vbCrLf
                Next

                File.WriteAllText(SaveFileDialog1.FileName, astring)
                'MessageBox.Show(astring)
            End If
        End If
    End Sub

    Private Sub btnOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpen.Click
        If OpenFileDialog2.ShowDialog = Windows.Forms.DialogResult.OK Then
            'Dim file As String
            Dim m3ufile As String
            m3ufile = My.Computer.FileSystem.ReadAllText(OpenFileDialog2.FileName)
            'MessageBox.Show(m3ufile)

            Dim result() As String = Split(m3ufile, vbCrLf)
            Dim row As Integer = result.Length
            'MsgBox(row)

            For c As Integer = 2 To row Step 2
                ListBoxSong.Items.Add(result(c))
            Next


            'For Each file In OpenFileDialog2.FileNames()
            '    ListBoxSong.Items.Add(file)
            'Next
        End If
    End Sub
End Class