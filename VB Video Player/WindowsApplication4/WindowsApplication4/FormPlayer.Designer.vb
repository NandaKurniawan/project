﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormPlayer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormPlayer))
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.SubsColor = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ChangeColor = New System.Windows.Forms.ToolStripMenuItem()
        Me.blue = New System.Windows.Forms.ToolStripMenuItem()
        Me.green = New System.Windows.Forms.ToolStripMenuItem()
        Me.yellow = New System.Windows.Forms.ToolStripMenuItem()
        Me.cyan = New System.Windows.Forms.ToolStripMenuItem()
        Me.WhiteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ChangeFontSizeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BiggerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SmallerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripStatusSongPath = New System.Windows.Forms.ToolStripStatusLabel()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.TimerPlay = New System.Windows.Forms.Timer(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PanelControl = New System.Windows.Forms.Panel()
        Me.btnFullscreen = New System.Windows.Forms.Button()
        Me.TrackBarVolume = New System.Windows.Forms.TrackBar()
        Me.lblDuration = New System.Windows.Forms.Label()
        Me.lblTimeElapsed = New System.Windows.Forms.Label()
        Me.btnPlay = New System.Windows.Forms.Button()
        Me.btnPlaylist = New System.Windows.Forms.Button()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.btnPrev = New System.Windows.Forms.Button()
        Me.btnStop = New System.Windows.Forms.Button()
        Me.TrackBarProgress = New System.Windows.Forms.TrackBar()
        Me.Player = New AxWMPLib.AxWindowsMediaPlayer()
        Me.TimerShow = New System.Windows.Forms.Timer(Me.components)
        Me.TimerHide = New System.Windows.Forms.Timer(Me.components)
        Me.TimerLeave = New System.Windows.Forms.Timer(Me.components)
        Me.TimerSubs = New System.Windows.Forms.Timer(Me.components)
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.StatusStrip1.SuspendLayout()
        Me.SubsColor.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.PanelControl.SuspendLayout()
        CType(Me.TrackBarVolume, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TrackBarProgress, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Player, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'StatusStrip1
        '
        Me.StatusStrip1.BackColor = System.Drawing.SystemColors.Highlight
        Me.StatusStrip1.ContextMenuStrip = Me.SubsColor
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusSongPath})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 419)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(624, 22)
        Me.StatusStrip1.TabIndex = 7
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'SubsColor
        '
        Me.SubsColor.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ChangeColor, Me.ChangeFontSizeToolStripMenuItem})
        Me.SubsColor.Name = "SubsColor"
        Me.SubsColor.Size = New System.Drawing.Size(176, 48)
        Me.SubsColor.Text = "Change Subs Color"
        '
        'ChangeColor
        '
        Me.ChangeColor.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.blue, Me.green, Me.yellow, Me.cyan, Me.WhiteToolStripMenuItem})
        Me.ChangeColor.Name = "ChangeColor"
        Me.ChangeColor.Size = New System.Drawing.Size(175, 22)
        Me.ChangeColor.Text = "Change Subs Color"
        '
        'blue
        '
        Me.blue.Name = "blue"
        Me.blue.Size = New System.Drawing.Size(109, 22)
        Me.blue.Text = "Blue"
        '
        'green
        '
        Me.green.Name = "green"
        Me.green.Size = New System.Drawing.Size(109, 22)
        Me.green.Text = "Green"
        '
        'yellow
        '
        Me.yellow.Name = "yellow"
        Me.yellow.Size = New System.Drawing.Size(109, 22)
        Me.yellow.Text = "Yellow"
        '
        'cyan
        '
        Me.cyan.Name = "cyan"
        Me.cyan.Size = New System.Drawing.Size(109, 22)
        Me.cyan.Text = "Cyan"
        '
        'WhiteToolStripMenuItem
        '
        Me.WhiteToolStripMenuItem.Name = "WhiteToolStripMenuItem"
        Me.WhiteToolStripMenuItem.Size = New System.Drawing.Size(109, 22)
        Me.WhiteToolStripMenuItem.Text = "White"
        '
        'ChangeFontSizeToolStripMenuItem
        '
        Me.ChangeFontSizeToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BiggerToolStripMenuItem, Me.SmallerToolStripMenuItem})
        Me.ChangeFontSizeToolStripMenuItem.Name = "ChangeFontSizeToolStripMenuItem"
        Me.ChangeFontSizeToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.ChangeFontSizeToolStripMenuItem.Text = "Change Font Size"
        '
        'BiggerToolStripMenuItem
        '
        Me.BiggerToolStripMenuItem.Name = "BiggerToolStripMenuItem"
        Me.BiggerToolStripMenuItem.Size = New System.Drawing.Size(113, 22)
        Me.BiggerToolStripMenuItem.Text = "Bigger"
        '
        'SmallerToolStripMenuItem
        '
        Me.SmallerToolStripMenuItem.Name = "SmallerToolStripMenuItem"
        Me.SmallerToolStripMenuItem.Size = New System.Drawing.Size(113, 22)
        Me.SmallerToolStripMenuItem.Text = "Smaller"
        '
        'ToolStripStatusSongPath
        '
        Me.ToolStripStatusSongPath.Name = "ToolStripStatusSongPath"
        Me.ToolStripStatusSongPath.Size = New System.Drawing.Size(42, 17)
        Me.ToolStripStatusSongPath.Text = "Ready."
        '
        'TimerPlay
        '
        '
        'Panel1
        '
        Me.Panel1.AutoSize = True
        Me.Panel1.Controls.Add(Me.PanelControl)
        Me.Panel1.Controls.Add(Me.Player)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(624, 419)
        Me.Panel1.TabIndex = 27
        '
        'PanelControl
        '
        Me.PanelControl.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelControl.ContextMenuStrip = Me.SubsColor
        Me.PanelControl.Controls.Add(Me.btnFullscreen)
        Me.PanelControl.Controls.Add(Me.TrackBarVolume)
        Me.PanelControl.Controls.Add(Me.lblDuration)
        Me.PanelControl.Controls.Add(Me.lblTimeElapsed)
        Me.PanelControl.Controls.Add(Me.btnPlay)
        Me.PanelControl.Controls.Add(Me.btnPlaylist)
        Me.PanelControl.Controls.Add(Me.btnNext)
        Me.PanelControl.Controls.Add(Me.btnPrev)
        Me.PanelControl.Controls.Add(Me.btnStop)
        Me.PanelControl.Controls.Add(Me.TrackBarProgress)
        Me.PanelControl.Location = New System.Drawing.Point(0, 329)
        Me.PanelControl.Name = "PanelControl"
        Me.PanelControl.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.PanelControl.Size = New System.Drawing.Size(624, 90)
        Me.PanelControl.TabIndex = 33
        '
        'btnFullscreen
        '
        Me.btnFullscreen.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFullscreen.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFullscreen.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.btnFullscreen.Location = New System.Drawing.Point(67, 49)
        Me.btnFullscreen.MaximumSize = New System.Drawing.Size(87, 20)
        Me.btnFullscreen.Name = "btnFullscreen"
        Me.btnFullscreen.Size = New System.Drawing.Size(87, 20)
        Me.btnFullscreen.TabIndex = 34
        Me.btnFullscreen.Text = "Full Screen"
        Me.btnFullscreen.UseVisualStyleBackColor = True
        '
        'TrackBarVolume
        '
        Me.TrackBarVolume.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.TrackBarVolume.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.TrackBarVolume.Location = New System.Drawing.Point(475, 43)
        Me.TrackBarVolume.Maximum = 100
        Me.TrackBarVolume.Name = "TrackBarVolume"
        Me.TrackBarVolume.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TrackBarVolume.Size = New System.Drawing.Size(76, 45)
        Me.TrackBarVolume.TabIndex = 8
        Me.TrackBarVolume.Value = 100
        '
        'lblDuration
        '
        Me.lblDuration.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDuration.AutoSize = True
        Me.lblDuration.Location = New System.Drawing.Point(572, 50)
        Me.lblDuration.Name = "lblDuration"
        Me.lblDuration.Size = New System.Drawing.Size(34, 13)
        Me.lblDuration.TabIndex = 7
        Me.lblDuration.Text = "00:00"
        '
        'lblTimeElapsed
        '
        Me.lblTimeElapsed.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTimeElapsed.AutoSize = True
        Me.lblTimeElapsed.Location = New System.Drawing.Point(12, 50)
        Me.lblTimeElapsed.Name = "lblTimeElapsed"
        Me.lblTimeElapsed.Size = New System.Drawing.Size(34, 13)
        Me.lblTimeElapsed.TabIndex = 6
        Me.lblTimeElapsed.Text = "00:00"
        '
        'btnPlay
        '
        Me.btnPlay.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnPlay.AutoSize = True
        Me.btnPlay.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.btnPlay.Location = New System.Drawing.Point(276, 38)
        Me.btnPlay.Name = "btnPlay"
        Me.btnPlay.Size = New System.Drawing.Size(74, 43)
        Me.btnPlay.TabIndex = 5
        Me.btnPlay.Text = "Play"
        Me.btnPlay.UseVisualStyleBackColor = True
        '
        'btnPlaylist
        '
        Me.btnPlaylist.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnPlaylist.AutoSize = True
        Me.btnPlaylist.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.btnPlaylist.Location = New System.Drawing.Point(405, 38)
        Me.btnPlaylist.Name = "btnPlaylist"
        Me.btnPlaylist.Size = New System.Drawing.Size(43, 43)
        Me.btnPlaylist.TabIndex = 4
        Me.btnPlaylist.Text = "PL"
        Me.btnPlaylist.UseVisualStyleBackColor = True
        '
        'btnNext
        '
        Me.btnNext.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnNext.AutoSize = True
        Me.btnNext.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.btnNext.Location = New System.Drawing.Point(356, 38)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(43, 43)
        Me.btnNext.TabIndex = 3
        Me.btnNext.Text = "Next"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'btnPrev
        '
        Me.btnPrev.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnPrev.AutoSize = True
        Me.btnPrev.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.btnPrev.Location = New System.Drawing.Point(227, 38)
        Me.btnPrev.Name = "btnPrev"
        Me.btnPrev.Size = New System.Drawing.Size(43, 43)
        Me.btnPrev.TabIndex = 2
        Me.btnPrev.Text = "Prev"
        Me.btnPrev.UseVisualStyleBackColor = True
        '
        'btnStop
        '
        Me.btnStop.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnStop.AutoSize = True
        Me.btnStop.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.btnStop.Location = New System.Drawing.Point(178, 38)
        Me.btnStop.Name = "btnStop"
        Me.btnStop.Size = New System.Drawing.Size(43, 43)
        Me.btnStop.TabIndex = 1
        Me.btnStop.Text = "Stop"
        Me.btnStop.UseVisualStyleBackColor = True
        '
        'TrackBarProgress
        '
        Me.TrackBarProgress.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TrackBarProgress.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.TrackBarProgress.Location = New System.Drawing.Point(4, 5)
        Me.TrackBarProgress.Name = "TrackBarProgress"
        Me.TrackBarProgress.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TrackBarProgress.Size = New System.Drawing.Size(617, 45)
        Me.TrackBarProgress.TabIndex = 0
        '
        'Player
        '
        Me.Player.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Player.Enabled = True
        Me.Player.Location = New System.Drawing.Point(0, 0)
        Me.Player.Name = "Player"
        Me.Player.OcxState = CType(resources.GetObject("Player.OcxState"), System.Windows.Forms.AxHost.State)
        Me.Player.Size = New System.Drawing.Size(624, 419)
        Me.Player.TabIndex = 32
        '
        'TimerShow
        '
        Me.TimerShow.Interval = 10
        '
        'TimerHide
        '
        Me.TimerHide.Interval = 10
        '
        'TimerLeave
        '
        Me.TimerLeave.Interval = 1000
        '
        'TimerSubs
        '
        Me.TimerSubs.Interval = 200
        '
        'SaveFileDialog1
        '
        Me.SaveFileDialog1.CreatePrompt = True
        Me.SaveFileDialog1.Title = "Save Capture"
        '
        'FormPlayer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ClientSize = New System.Drawing.Size(624, 441)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimumSize = New System.Drawing.Size(393, 275)
        Me.Name = "FormPlayer"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "12.03AM MediaPlayer"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.SubsColor.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.PanelControl.ResumeLayout(False)
        Me.PanelControl.PerformLayout()
        CType(Me.TrackBarVolume, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TrackBarProgress, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Player, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusSongPath As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents TimerPlay As System.Windows.Forms.Timer
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents TimerShow As System.Windows.Forms.Timer
    Friend WithEvents TimerHide As System.Windows.Forms.Timer
    Friend WithEvents TimerLeave As System.Windows.Forms.Timer
    Friend WithEvents TimerSubs As System.Windows.Forms.Timer
    Friend WithEvents Player As AxWMPLib.AxWindowsMediaPlayer
    Friend WithEvents PanelControl As System.Windows.Forms.Panel
    Friend WithEvents TrackBarVolume As System.Windows.Forms.TrackBar
    Friend WithEvents lblDuration As System.Windows.Forms.Label
    Friend WithEvents lblTimeElapsed As System.Windows.Forms.Label
    Friend WithEvents btnPlay As System.Windows.Forms.Button
    Friend WithEvents btnPlaylist As System.Windows.Forms.Button
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents btnPrev As System.Windows.Forms.Button
    Friend WithEvents btnStop As System.Windows.Forms.Button
    Friend WithEvents TrackBarProgress As System.Windows.Forms.TrackBar
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents btnFullscreen As System.Windows.Forms.Button
    Friend WithEvents SubsColor As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ChangeColor As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents blue As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents green As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents yellow As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cyan As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WhiteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ChangeFontSizeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BiggerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SmallerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
