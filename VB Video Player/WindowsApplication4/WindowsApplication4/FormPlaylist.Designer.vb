﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormPlaylist
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnRepeat = New System.Windows.Forms.Button()
        Me.btnRandom = New System.Windows.Forms.Button()
        Me.btnRemove = New System.Windows.Forms.Button()
        Me.btnOpenDir = New System.Windows.Forms.Button()
        Me.ListBoxSong = New System.Windows.Forms.ListBox()
        Me.btnAddFile = New System.Windows.Forms.Button()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnOpen = New System.Windows.Forms.Button()
        Me.OpenFileDialog2 = New System.Windows.Forms.OpenFileDialog()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.SuspendLayout()
        '
        'btnRepeat
        '
        Me.btnRepeat.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.btnRepeat.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.btnRepeat.Location = New System.Drawing.Point(220, 343)
        Me.btnRepeat.Name = "btnRepeat"
        Me.btnRepeat.Size = New System.Drawing.Size(43, 43)
        Me.btnRepeat.TabIndex = 15
        Me.btnRepeat.Text = "Rpt"
        Me.btnRepeat.UseVisualStyleBackColor = True
        '
        'btnRandom
        '
        Me.btnRandom.BackColor = System.Drawing.Color.White
        Me.btnRandom.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.btnRandom.Location = New System.Drawing.Point(171, 343)
        Me.btnRandom.Name = "btnRandom"
        Me.btnRandom.Size = New System.Drawing.Size(43, 43)
        Me.btnRandom.TabIndex = 14
        Me.btnRandom.Text = "Rndm"
        Me.btnRandom.UseVisualStyleBackColor = True
        '
        'btnRemove
        '
        Me.btnRemove.BackColor = System.Drawing.Color.White
        Me.btnRemove.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.btnRemove.Location = New System.Drawing.Point(122, 343)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(43, 43)
        Me.btnRemove.TabIndex = 13
        Me.btnRemove.Text = "Rem"
        Me.btnRemove.UseVisualStyleBackColor = True
        '
        'btnOpenDir
        '
        Me.btnOpenDir.BackColor = System.Drawing.Color.White
        Me.btnOpenDir.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.btnOpenDir.Location = New System.Drawing.Point(25, 343)
        Me.btnOpenDir.Name = "btnOpenDir"
        Me.btnOpenDir.Size = New System.Drawing.Size(43, 43)
        Me.btnOpenDir.TabIndex = 12
        Me.btnOpenDir.Text = "Open Dir"
        Me.btnOpenDir.UseVisualStyleBackColor = True
        '
        'ListBoxSong
        '
        Me.ListBoxSong.FormattingEnabled = True
        Me.ListBoxSong.HorizontalScrollbar = True
        Me.ListBoxSong.Location = New System.Drawing.Point(25, 37)
        Me.ListBoxSong.Name = "ListBoxSong"
        Me.ListBoxSong.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.ListBoxSong.Size = New System.Drawing.Size(238, 264)
        Me.ListBoxSong.TabIndex = 16
        '
        'btnAddFile
        '
        Me.btnAddFile.BackColor = System.Drawing.Color.White
        Me.btnAddFile.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.btnAddFile.Location = New System.Drawing.Point(73, 343)
        Me.btnAddFile.Name = "btnAddFile"
        Me.btnAddFile.Size = New System.Drawing.Size(43, 43)
        Me.btnAddFile.TabIndex = 17
        Me.btnAddFile.Text = "Add File"
        Me.btnAddFile.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        Me.OpenFileDialog1.Multiselect = True
        '
        'btnSave
        '
        Me.btnSave.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.btnSave.Location = New System.Drawing.Point(145, 307)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(118, 30)
        Me.btnSave.TabIndex = 19
        Me.btnSave.Text = "Save PL"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnOpen
        '
        Me.btnOpen.ForeColor = System.Drawing.Color.DeepSkyBlue
        Me.btnOpen.Location = New System.Drawing.Point(25, 307)
        Me.btnOpen.Name = "btnOpen"
        Me.btnOpen.Size = New System.Drawing.Size(118, 30)
        Me.btnOpen.TabIndex = 20
        Me.btnOpen.Text = "Open PL"
        Me.btnOpen.UseVisualStyleBackColor = True
        '
        'OpenFileDialog2
        '
        Me.OpenFileDialog2.DefaultExt = "m3u files | *.m3u"
        Me.OpenFileDialog2.FileName = "OpenFileDialog2"
        Me.OpenFileDialog2.Filter = "m3u files | *.m3u"
        '
        'SaveFileDialog1
        '
        Me.SaveFileDialog1.DefaultExt = "m3u files | *.m3u"
        Me.SaveFileDialog1.Filter = "m3u files | *.m3u"
        '
        'FormPlaylist
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ClientSize = New System.Drawing.Size(292, 407)
        Me.Controls.Add(Me.btnOpen)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnAddFile)
        Me.Controls.Add(Me.ListBoxSong)
        Me.Controls.Add(Me.btnRepeat)
        Me.Controls.Add(Me.btnRandom)
        Me.Controls.Add(Me.btnRemove)
        Me.Controls.Add(Me.btnOpenDir)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.Name = "FormPlaylist"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "12.03AM Playlist"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnRepeat As System.Windows.Forms.Button
    Friend WithEvents btnRandom As System.Windows.Forms.Button
    Friend WithEvents btnRemove As System.Windows.Forms.Button
    Friend WithEvents btnOpenDir As System.Windows.Forms.Button
    Friend WithEvents ListBoxSong As System.Windows.Forms.ListBox
    Friend WithEvents btnAddFile As System.Windows.Forms.Button
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnOpen As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog2 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
End Class
